#ifndef UTWC_COMMAND_CHAIN
#define UTWC_COMMAND_CHAIN

#include <stdbool.h>

struct utwc_command;

/**
 * A dynamically allocated array of commands to be executed in order.
 */
struct utwc_command_chain {
	struct utwc_command *start;
	unsigned int length;
};

#endif // UTWC_COMMAND_CHAIN
