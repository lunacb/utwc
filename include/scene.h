#ifndef UTWC_SCENE_H
#define UTWC_SCENE_H

#include <pixman.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_layer_shell_v1.h>

struct wlr_output;
struct wlr_output_layout;
struct wlr_xdg_surface;

enum utwc_scene_node_type {
	UTWC_SCENE_NODE_ROOT,
	UTWC_SCENE_NODE_TREE,
	UTWC_SCENE_NODE_SURFACE,
	UTWC_SCENE_NODE_VIEW,
};

enum utwc_border {
	UTWC_BORDER_UP,
	UTWC_BORDER_DOWN,
	UTWC_BORDER_LEFT,
	UTWC_BORDER_RIGHT,
};

struct utwc_scene_node_state {
	struct wl_list link; // utwc_scene_node_state.children

	struct wl_list children; // utwc_scene_node_state.link

	bool enabled;
	// crop from enforced size for utwc_scene_view
	bool inherit_crop;
	int x, y; // relative to parent
};

/** A node is an object in the scene. */
struct utwc_scene_node {
	enum utwc_scene_node_type type;
	struct utwc_scene_node *parent;
	struct utwc_scene_node_state state;

	struct {
		struct wl_signal destroy;
	} events;

	void *data;
};

/** The root scene-graph node. */
struct utwc_G_scene {
	bool created;
	struct utwc_scene_node node;
};

/** A sub-tree in the scene-graph. */
struct utwc_scene_tree {
	struct utwc_scene_node node;
};

/** A scene-graph node displaying a single surface. */
struct utwc_scene_surface {
	struct utwc_scene_node node;
	struct wlr_surface *surface;

	// private state

	int prev_width, prev_height;

	struct wl_listener surface_destroy;
	struct wl_listener surface_commit;
};

struct utwc_scene_view_border {
	unsigned int width[4];
	float color[4];
};

struct utwc_scene_view {
	struct utwc_scene_node node;

	struct utwc_scene_node *tree;
	struct wlr_xdg_surface *xdg_surface;

	struct utwc_scene_view_border border;

	unsigned forced_width;
	unsigned int forced_height;

	// private state

	unsigned int real_width;
	unsigned int real_height;

	struct wlr_box crop;

	struct wl_listener xdg_surface_destroy;
	struct wl_listener xdg_surface_map;
	struct wl_listener xdg_surface_unmap;
	struct wl_listener xdg_surface_commit;
};

/** A layer shell scene helper */
struct utwc_scene_layer_surface_v1 {
	struct utwc_scene_tree *tree;
	struct wlr_layer_surface_v1 *layer_surface;

	// private state

	struct wl_listener tree_destroy;
	struct wl_listener layer_surface_destroy;
	struct wl_listener layer_surface_map;
	struct wl_listener layer_surface_unmap;
};


typedef void (*utwc_scene_node_iterator_func_t)(struct utwc_scene_node *node,
	int sx, int sy, void *data);

/**
 * Immediately destroy the scene-graph node.
 */
void utwc_scene_node_destroy(struct utwc_scene_node *node);
/**
 * Enable or disable this node. If a node is disabled, all of its children are
 * implicitly disabled as well.
 */
void utwc_scene_node_set_enabled(struct utwc_scene_node *node, bool enabled);
void utwc_scene_node_set_inherit_crop(struct utwc_scene_node *node,
		bool inherit_crop);
/**
 * Set the position of the node relative to its parent.
 */
void utwc_scene_node_set_position(struct utwc_scene_node *node, int x, int y);
/**
 * Move the node right above the specified sibling.
 * Asserts that node and sibling are distinct and share the same parent.
 */
void utwc_scene_node_place_above(struct utwc_scene_node *node,
	struct utwc_scene_node *sibling);
/**
 * Move the node right below the specified sibling.
 * Asserts that node and sibling are distinct and share the same parent.
 */
void utwc_scene_node_place_below(struct utwc_scene_node *node,
	struct utwc_scene_node *sibling);
/**
 * Move the node above all of its sibling nodes.
 */
void utwc_scene_node_raise_to_top(struct utwc_scene_node *node);
/**
 * Move the node below all of its sibling nodes.
 */
void utwc_scene_node_lower_to_bottom(struct utwc_scene_node *node);
/**
 * Move the node to another location in the tree.
 */
void utwc_scene_node_reparent(struct utwc_scene_node *node,
	struct utwc_scene_node *new_parent);
/**
 * Get the node's layout-local coordinates.
 *
 * True is returned if the node and all of its ancestors are enabled.
 */
bool utwc_scene_node_coords(struct utwc_scene_node *node, int *lx, int *ly);
/**
 * Call `iterator` on each surface in the scene-graph, with the surface's
 * position in layout coordinates. The function is called from root to leaves
 * (in rendering order).
 */
void utwc_scene_node_for_each_surface(struct utwc_scene_node *node,
	wlr_surface_iterator_func_t iterator, void *user_data);
/**
 * Find the topmost node in this scene-graph that contains the point at the
 * given layout-local coordinates. (For surface nodes, this means accepting
 * input events at that point.) Returns the node and coordinates relative to the
 * returned node, or NULL if no node is found at that location.
 */
struct utwc_scene_node *utwc_scene_node_at(struct utwc_scene_node *node,
	double lx, double ly, double *nx, double *ny);

/**
 * Create a new scene-graph.
 */
bool utwc_G_scene_init(void);
/**
 * Manually render the scene-graph on an output. The compositor needs to call
 * wlr_renderer_begin before and wlr_renderer_end after calling this function.
 * Damage is given in output-buffer-local coordinates and can be set to NULL to
 * disable damage tracking.
 */
void utwc_scene_render_output(pixman_region32_t *damage);

/**
 * Add a node displaying nothing but its children.
 */
struct utwc_scene_tree *utwc_scene_tree_create(struct utwc_scene_node *parent);

/**
 * Add a node displaying a single surface to the scene-graph.
 *
 * The child sub-surfaces are ignored.
 *
 * wlr_surface_send_enter()/wlr_surface_send_leave() will be called
 * automatically based on the position of the surface and outputs in
 * the scene.
 */
struct utwc_scene_surface *utwc_scene_surface_create(struct utwc_scene_node *parent,
	struct wlr_surface *surface);

struct utwc_scene_surface *utwc_scene_surface_from_node(struct utwc_scene_node *node);

/**
 * Add a viewport for the specified output to the scene-graph.
 *
 * An output can only be added once to the scene-graph.
 */
bool utwc_scene_output_create(struct wlr_output *wlr_output);
/**
 * Destroy a scene-graph output.
 */
void utwc_scene_output_destroy(void);
/**
 * Render and commit an output.
 */
bool utwc_scene_output_commit(void);
/**
 * Call wlr_surface_send_frame_done() on all surfaces in the scene rendered by
 * utwc_scene_output_commit() if there is an output.
 */
void utwc_scene_output_send_frame_done(struct timespec *now);
/**
 * Call `iterator` on each surface in the scene-graph visible on the output,
 * with the surface's position in layout coordinates. The function is called
 * from root to leaves (in rendering order).
 */
// XXX: rename to just for_each_surface
void utwc_scene_output_for_each_surface(wlr_surface_iterator_func_t iterator,
		void *user_data);

/**
 * Add a node displaying a surface and all of its sub-surfaces to the
 * scene-graph.
 */
struct utwc_scene_node *utwc_scene_subsurface_tree_create(
	struct utwc_scene_node *parent, struct wlr_surface *surface);

/**
 * Add a node displaying an xdg_surface and all of its sub-surfaces to the
 * scene-graph.
 *
 * The origin of the returned scene-graph node will match the top-left corner
 * of the xdg_surface window geometry.
 */
struct utwc_scene_node *utwc_scene_xdg_surface_create(
	struct utwc_scene_node *parent, struct wlr_xdg_surface *xdg_surface);

/**
 * Add a node wrapping arond an xdg_surface node that will display a border
 * around it and will enforce a given size for that surface by centering and
 * cropping it.
 *
 * wlr_xdg_toplevel_set_size will be called appropriately based on the size.
 */
struct utwc_scene_view *utwc_scene_view_create(struct utwc_scene_node *parent,
		struct wlr_xdg_surface *xdg_surface);

/**
 * width and height may be zero to not enforce a size.
 */
void utwc_scene_view_set_size(struct utwc_scene_view *view, int width,
		int height);

void utwc_scene_view_set_border_width(struct utwc_scene_view *view,
		unsigned int width[4]);

void utwc_scene_view_set_border_color(struct utwc_scene_view *view,
		float color[4]);

/**
 * Add a node displaying a layer_surface_v1 and all of its sub-surfaces to the
 * scene-graph.
 *
 * The origin of the returned scene-graph node will match the top-left corner
 * of the layer surface.
 */
struct utwc_scene_layer_surface_v1 *utwc_scene_layer_surface_v1_create(
	struct utwc_scene_node *parent, struct wlr_layer_surface_v1 *layer_surface);

/**
 * Configure a layer_surface_v1, position its scene node in accordance to its
 * current state, and update the remaining usable area.
 *
 * full_area represents the entire area that may be used by the layer surface
 * if its exclusive_zone is -1, and is usually the output dimensions.
 * usable_area represents what remains of full_area that can be used if
 * exclusive_zone is >= 0. usable_area is updated if the surface has a positive
 * exclusive_zone, so that it can be used for the next layer surface.
 */
void utwc_scene_layer_surface_v1_configure(
	struct utwc_scene_layer_surface_v1 *scene_layer_surface,
	const struct wlr_box *full_area, struct wlr_box *usable_area);

extern struct utwc_G_scene G_scene;

#endif // UTWC_SCENE_H
