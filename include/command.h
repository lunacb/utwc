#ifndef UTWC_COMAND_H
#define UTWC_COMAND_H

#include <unistd.h>
#include "command_chain.h"
#include "desktop.h"
#include "input/gesture.h"
#include "input/keybind.h"
#include "rule.h"

enum utwc_command_type {
	UTWC_COMMAND_BINDGESTURE,
	UTWC_COMMAND_BINDSYM,
	UTWC_COMMAND_EXEC,
	UTWC_COMMAND_FOCUS,
	UTWC_COMMAND_FOCUS_NEXT,
	UTWC_COMMAND_FOCUS_PREV,
	UTWC_COMMAND_KILL,
	UTWC_COMMAND_MARK,
	UTWC_COMMAND_QUIT,
	UTWC_COMMAND_RESIZE,
	UTWC_COMMAND_UNMARK,
	UTWC_COMMAND_WATCH,
	UTWC_COMMAND_WORKSPACE,
	UTWC_COMMAND_WORKSPACE_CYCLE,
	UTWC_COMMAND_WORKSPACE_NEXT,
	UTWC_COMMAND_WORKSPACE_PREV,
};

enum utwc_target_specifier_type {
	UTWC_TARGET_WATCHER,
	UTWC_TARGET_RULE,
};

enum utwc_rule_executor_type {
	UTWC_RULE_EXECUTOR_NONE,
	UTWC_RULE_EXECUTOR_SINGLE,
};

/**
 * Use either a watcher, which will already have windows matched, or a new
 * rule, which will look for any matching targets.
 */
struct utwc_target_specifier {
	enum utwc_target_specifier_type type;
	union {
		struct utwc_rule_watcher *watcher;

		struct {
			struct utwc_rule rule;
			bool foreach;
		} rule;
	};
};

struct utwc_command_bindgesture {
	bool has_start_rule, has_end_rule;
	struct utwc_rule start_rule;
	struct utwc_rule end_rule;

	struct utwc_gesture_binding binding;
	char *mode_name;
	char *mode_title;
};

struct utwc_command_bindsym {
	struct utwc_keybinding binding;
	char *mode_name;
	char *mode_title;
};

struct utwc_command_exec {
	// Formatted as the `argv` argument in exec functions. Both `cmdline` and
	// `cmdline[0]` are alloc'd pointers.
	char **cmdline;
	// A mark given to the first window created by the PID. Can be NULL for no
	// mark.
	bool has_mark;
	char mark[MARK_NAME_MAX];
};

struct utwc_command_focus {
	struct utwc_target_specifier specifier;
};

struct utwc_command_focus_next {

};

struct utwc_command_focus_prev {

};

struct utwc_command_kill {
	struct utwc_target_specifier specifier;
};

struct utwc_command_mark {
	struct utwc_target_specifier specifier;
	char *mark;
};

struct utwc_command_quit {

};

struct utwc_command_resize {
	struct utwc_target_specifier specifier;
	char *mark;
	// TODO: revise
	int x, y, width, height;
};

struct utwc_command_unmark {
	struct utwc_target_specifier specifier;
	char *mark;
};

struct utwc_command_workspace {
	char *mark;
};

struct utwc_command_workspace_cycle {
	// . Both `marks` and `marks[0]` are alloc'd pointers.
	char **marks;
	bool reverse;
};

struct utwc_command_workspace_next {

};

struct utwc_command_workspace_prev {

};

struct utwc_command_watch {
	struct utwc_target_specifier specifier;
	struct utwc_command_chain command;
};

struct utwc_command {
	enum utwc_command_type type;

	union {
		struct utwc_command_bindgesture bindgesture;
		struct utwc_command_bindsym bindsym;
		struct utwc_command_exec exec;
		struct utwc_command_focus focus;
		struct utwc_command_focus_next focus_next;
		struct utwc_command_focus_prev focus_prev;
		struct utwc_command_kill kill;
		struct utwc_command_mark mark;
		struct utwc_command_quit quit;
		struct utwc_command_resize resize;
		struct utwc_command_unmark unmark;
		struct utwc_command_watch watch;
		struct utwc_command_workspace workspace;
		struct utwc_command_workspace_cycle workspace_cycle;
		struct utwc_command_workspace_next workspace_next;
		struct utwc_command_workspace_prev workspace_prev;
	};
};

struct utwc_rule_watcher;

/**
 * Decides what to do if a `struct utwc_rule_watcher` finds a new matching
 * target
 */
struct utwc_rule_executor {
	enum utwc_rule_executor_type type;
	union {
		// Execute a command.
		struct utwc_command_chain single;
	};
};

/**
 * A single target currently matching a rule held by a watcher.
 */
struct utwc_rule_index {
	struct utwc_rule_watcher *watcher;
	struct utwc_rule_target *target;
	struct wl_list link;

	struct wl_listener e_destroy;
};

/**
 * Watches for targets that match the given `rule` and maintains a list of
 * matching targets. Triggers `executor` when a target is found.
 */
struct utwc_rule_watcher {
	struct utwc_rule rule;
	struct utwc_rule_executor executor;

	struct wl_list matched; // struct utwc_rule_index
	struct wl_list link;
};

struct utwc_pid_mark {
	pid_t pid;
	char name[MARK_NAME_MAX];
};

struct utwc_G_command {
	bool created;
	struct wl_list watchers;
	// It is assumed that there will never be any duplicate PID's in this
	// array.
	struct wl_array pid_marks; // struct utwc_pid_mark
};

bool utwc_G_command_init(void);

void utwc_G_command_fini(void);

void utwc_command_chain_execute(struct utwc_command_chain *chain);

/**
 * Start listening for targets with a watcher. A copy will be created by the
 * function.
 */
struct utwc_rule_watcher *utwc_watcher_add(struct utwc_rule_watcher *watcher);

void utwc_watcher_fini(struct utwc_rule_watcher *watcher);

/**
 * Notify watchers that a target has changed properties that may have rules
 * apply to them
 */
void utwc_watcher_notify_rule_target(struct utwc_rule_target *target);

/**
 * Look for an entry in `G_command.pid_marks` that matches the given pid. If
 * found, remove the entry, copy the mark name to `buf` if it isn't NULL, and
 * return true. Otherwise return false.
 */
bool utwc_pid_mark_pull(pid_t pid, char *buf);

void utwc_command_chain_fini(struct utwc_command_chain *chain);

extern struct utwc_G_command G_command;

#endif // UTWC_COMAND_H
