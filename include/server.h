#ifndef UTWC_SERVER_H
#define UTWC_SERVER_H

#include <stdbool.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_presentation_time.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_xdg_output_v1.h>
#include <wlr/types/wlr_virtual_keyboard_v1.h>

#include "output.h"
#include "input/seat.h"

struct utwc_G_server {
	struct wl_display *wl_display;
	struct wlr_backend *wlr_backend;
	struct wlr_compositor *wlr_compositor;
	struct wlr_renderer *wlr_renderer;
	struct wlr_xdg_shell *wlr_xdg_shell;
	struct wlr_allocator *wlr_allocator;
	struct wlr_presentation *wlr_presentation;
	struct wlr_layer_shell_v1 *wlr_layer_shell;
	struct wlr_output_layout *wlr_output_layout;
	struct wlr_xdg_output_manager_v1 *wlr_xdg_output_manager;
	struct wlr_virtual_keyboard_manager_v1 *wlr_virtual_keyboard_manager;
};

extern struct utwc_G_server G_server;

bool utwc_server_init(void);

void utwc_server_run(void);

void utwc_server_destroy(void);

#endif
