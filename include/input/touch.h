#define _POSIX_C_SOURCE 201112L

#ifndef UTWC_TOUCH_H
#define UTWC_TOUCH_H

#include <stdbool.h>
#include <stddef.h>
#include <time.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_input_device.h>
#include "command.h"

// A touch device discovered before there was an output.
struct utwc_pending_touch {
	struct wl_list link;
	struct wlr_input_device *device;
};

struct utwc_G_touch {
	struct wlr_input_device *device;
	struct wl_list pending_devices;

	// Touch id pretending to be a pointer for pointer simulation.
	bool has_pointer_id;
	int32_t pointer_id;

	struct wl_listener e_down;
	struct wl_listener e_motion;
	struct wl_listener e_up;
	struct wl_listener e_frame;
	struct wl_listener e_destroy;
};

extern struct utwc_G_touch G_touch;

void utwc_touch_handle_new(struct wlr_input_device *device);

void utwc_touch_handle_pending(void);

#endif
