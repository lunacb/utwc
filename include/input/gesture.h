#ifndef UTWC_GESTURE_H
#define UTWC_GESTURE_H

#include <stdbool.h>
#include <time.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_touch.h>
#include "command_chain.h"
#include "input/binding.h"
#include "rule.h"

enum utwc_gesture_region {
	GESTURE_DIR_N = 1 << 0,
	GESTURE_DIR_S = 1 << 1,
	GESTURE_DIR_E = 1 << 2,
	GESTURE_DIR_W = 1 << 3,

	GESTURE_EDGE_N = 1 << 4,
	GESTURE_EDGE_S = 1 << 5,
	GESTURE_EDGE_E = 1 << 6,
	GESTURE_EDGE_W = 1 << 7,

	GESTURE_REL_N = 1 << 8,
	GESTURE_REL_S = 1 << 9,
	GESTURE_REL_E = 1 << 10,
	GESTURE_REL_W = 1 << 11,
	GESTURE_REL_NE = 1 << 12,
	GESTURE_REL_NW = 1 << 13,
	GESTURE_REL_SE = 1 << 14,
	GESTURE_REL_SW = 1 << 15,

	GESTURE_EDGE = 1 << 16,
	GESTURE_CENTER = 1 << 17,

	// If none of GESTURE_EDGE_* are set.
	GESTURE_MIDDLE_EDGE = 1 << 18,
};

enum utwc_gesture_surface {
	// A point is on a surface that can recieve input.
	GESTURE_ON_SURFACE,
	// Opposite of previous.
	GESTURE_ON_GAPS,
	// Either of the previous two.
	GESTURE_ON_ANY,
};

struct utwc_gesture_binding {
	// Masks of all the GESTURE_* conditions that must be true for the binding
	// to match.
	enum utwc_gesture_region start;
	enum utwc_gesture_region end;

	enum utwc_gesture_surface surface_start;
	enum utwc_gesture_surface surface_end;

	bool tap;

	unsigned int fingers;

	// The `tv_sec` field can be -1 to indicate no minimum/maximum time.
	struct timespec min_time;
	struct timespec max_time;

	struct utwc_command_chain command;

	// private state

	// To decide whether a binding could be executed for the currently down
	// fingers.
	bool invalidated;

	// Human-readable text representation of the binding.
	char human[128];

	struct wl_list link;

	// Can be NULL for no rule.
	struct utwc_rule_watcher *surface_start_rule;
	struct utwc_rule_watcher *surface_end_rule;
};


/**
 * `start_rule` and `end_rule` can be NULL. Returns true if the binding already
 * exists. The function owns anything alloc'd in `binding`, `start_rule`, and
 * `end_rule`.
 */
bool utwc_gesture_add_binding(struct utwc_gesture_binding *binding,
		struct utwc_binding_mode *mode, struct utwc_rule *start_rule,
		struct utwc_rule *end_rule);

/**
 * Returns true if the binding existed.
 */
bool utwc_gesture_remove_binding(struct utwc_gesture_binding *binding,
		struct utwc_binding_mode *mode, struct utwc_rule *start_rule,
		struct utwc_rule *end_rule);

/**
 * NOTE: this does not free associated watchers.
 */
void utwc_gesture_fini(struct utwc_gesture_binding *binding);

struct wlr_surface *utwc_gesture_do_touch_down(
		struct wlr_event_touch_down *event, int *ret_x, int *ret_y);

struct wlr_surface *utwc_gesture_do_touch_motion(
		struct wlr_event_touch_motion *event, int *ret_x, int *ret_y);

bool utwc_gesture_do_touch_up(struct wlr_event_touch_up *event);

// Called after the binding mode changes.
void utwc_gesture_update_for_mode(void);

#endif // UTWC_GESTURE_H
