#ifndef UTWC_KEYBOARD_H
#define UTWC_KEYBOARD_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_input_device.h>
#include "input/seat.h"

struct utwc_keyboard {
	struct wl_list link;

	struct wlr_input_device *device;

	struct wl_listener e_modifiers;
	struct wl_listener e_key;
	struct wl_listener e_destroy;
};

void utwc_keyboard_handle_new(struct wlr_input_device *device);

void utwc_keyboard_fini(struct utwc_keyboard *keyboard);

#endif
