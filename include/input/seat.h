#ifndef UTWC_SEAT_H
#define UTWC_SEAT_H

#include <stdbool.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_seat.h>
#include "rule.h"
#include "scene.h"

struct utwc_G_seat {
	bool created;

	struct wlr_seat *wlr_seat;

	struct wl_list keyboards; // struct utwc_keyboard

	struct wl_listener e_new_input;
	struct wl_listener e_new_virtual_keyboard;
	struct wl_listener e_request_cursor;
	struct wl_listener e_request_set_selection;
	struct wl_listener e_request_set_primary_selection;
	struct wl_listener e_request_start_drag;
	struct wl_listener e_start_drag;

	// Currently focused layer in G_layers.
	struct utwc_scene_node *layer;
};

extern struct utwc_G_seat G_seat;

bool utwc_G_seat_init(void);

void utwc_G_seat_fini(void);

void utwc_process_input(struct wlr_input_device *device);

/**
 * Check if the given `rule_target` is allowed to have keyboard focus
 */
bool seat_can_focus_keyboard(struct utwc_rule_target *rule_target);

#endif
