#ifndef UTWC_KEYBIND_H
#define UTWC_KEYBIND_H

#include <stddef.h>
#include <wayland-server-core.h>
#include <xkbcommon/xkbcommon.h>
#include "command_chain.h"
#include "input/binding.h"
#include "input/keyboard.h"

struct utwc_keybinding {
	uint32_t modifiers;
	xkb_keysym_t keysym;
	struct utwc_command_chain command;

	// private state

	// Human-readable text representation of the binding.
	char human[128];

	struct wl_list link;
};

/**
 * Returns true if the binding already exists. The function owns anything
 * alloc'd inside `binding`.
 */
bool utwc_keybind_add_binding(struct utwc_keybinding *binding,
		struct utwc_binding_mode *mode);

bool utwc_keybind_remove_binding(struct utwc_keybinding *binding,
		struct utwc_binding_mode *mode);

void utwc_keybind_fini(struct utwc_keybinding *binding);

bool utwc_keybind_do_key(const xkb_keysym_t *keysyms, size_t nsyms,
		uint32_t modifiers);

#endif // UTWC_KEYBIND_H
