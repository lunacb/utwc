#ifndef UTWC_BINDING_H
#define UTWC_BINDING_H

#include <stdbool.h>
#include <wayland-server-core.h>

#define UTWC_BINDING_STACK_MAX 20

struct utwc_binding_mode {
	char *name;
	char *title;

	struct wl_list gesture; // struct utwc_gesture_binding
	struct wl_list keybind; // struct utwc_keybinding

	struct wl_list link;
};

struct utwc_G_binding {
	bool created;

	struct wl_list modes; // struct utwc_binding_mode
	struct utwc_binding_mode *current;
};

bool utwc_G_binding_init(void);

void utwc_G_binding_fini(void);

/**
 * Searches for the mode named `mode`. If found, switch to it and return true.
 * Otherwise, return false.
 */
bool utwc_binding_name_enter(char *mode);

/**
 * Switch to the given binding.
 */
void utwc_binding_mode_enter(struct utwc_binding_mode *mode);

void utwc_binding_mode_remove(struct utwc_binding_mode *mode);

/**
 * Returns true if the binding existed.
 */
bool utwc_binding_name_remove(char *mode);

/**
 * `name` can be NULL to have no identifier. `title` will be presented to the
 * use when the binding is active if it is not NULL. The function will create
 * its own copies of `name` and `title`. Returns the found or created binding
 * mode.
 */
struct utwc_binding_mode *find_or_create_mode(char *name, char *title);

extern struct utwc_G_binding G_binding;

#endif // UTWC_BINDING_H
