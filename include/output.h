#ifndef UTWC_OUTPUT_H
#define UTWC_OUTPUT_H

#include <stddef.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_damage.h>
#include <wayland-server-core.h>

struct utwc_G_output {
	struct wlr_output *wlr_output;
	struct wlr_output_damage *damage;
	bool prev_scanout;

	struct wl_listener e_mode;
	struct wl_listener e_damage_frame;
	struct wl_listener e_damage_destroy;

	struct wl_listener e_new_output;
	struct wl_listener e_destroy;
};

extern struct utwc_G_output G_output;

bool utwc_G_output_init(void);

#endif
