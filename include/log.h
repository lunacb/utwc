#ifndef UTWC_LOG_H
#define UTWC_LOG_H

#include <errno.h>
#include <stdarg.h>
#include <string.h>

enum utwc_log_verbosity {
	UTWC_LOG_SILENT,
	UTWC_LOG_ERROR,
	UTWC_LOG_INFO,
	UTWC_LOG_DEBUG,
};

#ifdef __GNUC__
#define _UTWC_ATTRIB_PRINTF(start, end) __attribute__((format(printf, start, end)))
#else
#define _UTWC_ATTRIB_PRINTF(start, end)
#endif

void utwc_log_init(enum utwc_log_verbosity new_verb);

void utwc_log_va(enum utwc_log_verbosity verb,
		const char *file, int line,
		const char *format, va_list args) \
	     _UTWC_ATTRIB_PRINTF(4, 0);

void utwc_log(enum utwc_log_verbosity verb,
		const char *file, int line,
		const char *format, ...) \
	     _UTWC_ATTRIB_PRINTF(4, 5);

#define UTWC_LOG(verb, ...) \
	utwc_log(verb, __FILE__, __LINE__, __VA_ARGS__)
#define UTWC_LOG_VA(verb, ...) \
	utwc_log_va(verb, __FILE__, __LINE__, __VA_ARGS__)
#define UTWC_LOG_ERRNO(verb, fmt, ...) \
	utwc_log(verb, __FILE__, __LINE__, fmt ": %s", ## __VA_ARGS__, strerror(errno))

#endif
