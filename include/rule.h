#ifndef UTWC_RULE_H
#define UTWC_RULE_H

#include <stdbool.h>
#include <unistd.h>
#include <wayland-server-core.h>

enum utwc_rule_target_type {
	UTWC_RULE_VIEW,
	UTWC_RULE_LAYER_SURFACE,
};

enum utwc_rule_shell {
	UTWC_RULE_SHELL_XDG,
	UTWC_RULE_SHELL_LAYER,
};

enum utwc_rule_type {
	UTWC_RULE_TYPE_APP_ID,
	UTWC_RULE_TYPE_TITLE,
	UTWC_RULE_TYPE_SHELL,

	UTWC_RULE_TYPE_FOCUS,
	UTWC_RULE_TYPE_MARK,

	UTWC_RULE_TYPE_PID,
};

/**
 * Should only be located inside of a struct specified by the given `type`.
 */
struct utwc_rule_target {
	enum utwc_rule_target_type type;
};

struct utwc_rule_item {
	enum utwc_rule_type type;

	union {
		char *app_id;
		char *title;
		enum utwc_rule_shell shell;

		char *mark;

		pid_t pid;
	};
};

/**
 * A dynamically allocated list of rule items. All rule items must match for
 * the rule to match.
 */
struct utwc_rule {
	struct utwc_rule_item *start;
	unsigned int length;
};

/**
 * Test if the `target` matches the `rule`.
 */
bool utwc_rule_test(struct utwc_rule *rule, struct utwc_rule_target *target);

/**
 * Get a signal that can be listened for to detect the destruction of the
 * `target`.
 */
struct wl_signal *utwc_rule_target_get_destroy(struct utwc_rule_target *target);

bool utwc_rule_eq(struct utwc_rule *first, struct utwc_rule *second);

void utwc_rule_fini(struct utwc_rule *rule);

pid_t utwc_rule_target_get_pid(struct utwc_rule_target *target);

#endif // RULE_H
