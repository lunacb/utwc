#ifndef UTWC_LAYERS_H
#define UTWC_LAYERS_H

#include <wayland-server-core.h>
#include <wlr/util/box.h>
#include "rule.h"
#include "scene.h"

// For wlr-layer-shell support.
struct utwc_G_layers {
	struct utwc_scene_node *background;
	struct utwc_scene_node *bottom;
	struct utwc_scene_node *view;
	struct utwc_scene_node *top;
	struct utwc_scene_node *overlay;

	bool configured;
	struct wlr_box full_area;
	struct wlr_box usable_area;

	struct wl_listener e_new_surface;
};

struct utwc_layer_surface {
	struct utwc_scene_layer_surface_v1 *scene_surface;
	struct utwc_rule_target rule_target;
	struct utwc_scene_node *layer;

	struct wl_listener e_map;
	struct wl_listener e_unmap;
};

bool utwc_G_layers_init(void);

/**
 * Set the full area usable by layer_shell surface. This will reconfigure all
 * current surfaces and update G_layers.usable_area.
 */
void utwc_layer_surface_set_configure(struct wlr_box *full_area);

void utwc_layer_surface_unset_configure(void);

extern struct utwc_G_layers G_layers;

#endif // UTWC_LAYERS_H
