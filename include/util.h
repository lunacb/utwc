#ifndef UTWC_UTIL_C
#define UTWC_UTIL_C

#include <stddef.h>
#include <stdlib.h>
#include <wayland-server-core.h>

#define HANDLE_SIGNAL(listener, func, signal) \
	(listener).notify = func; \
	wl_signal_add(&(signal), &(listener));

void *alloc_test(const char *file, int line, void *ptr);
void free_list_offst(struct wl_list *head, size_t offst);

#define free_list(head, member, type) \
	free_list_offst(head, offsetof(type, member))

#define utwc_malloc(size) alloc_test(__FILE__, __LINE__, malloc(size))
#define utwc_realloc(ptr, size) alloc_test(__FILE__, __LINE__, realloc(ptr, size))
#define utwc_calloc(nmemb, size) alloc_test(__FILE__, __LINE__, calloc(nmemb, size))
#define utwc_strdup(str) alloc_test(__FILE__, __LINE__, strdup(str))

#define ARRLEN(arr) \
	(sizeof(arr) / sizeof(*(arr)))

#endif
