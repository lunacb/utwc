#ifndef UTWC_DESKTOP_H
#define UTWC_DESKTOP_H

#define MARK_NAME_MAX 64

#include <stdbool.h>
#include <wayland-server-core.h>
#include <wlr/util/box.h>
#include "rule.h"
#include "scene.h"

/**
 * A mark with a list of views that have that mark.
 */
struct utwc_mark {
	char name[MARK_NAME_MAX];
	struct wl_list views; // struct utwc_view_mark

	struct wl_list link;
};

struct utwc_G_desktop {
	bool created;

	struct wl_list views; // struct utwc_view
	// Currently focused view.
	struct utwc_view *activated;
	// Global list of all marks.
	struct wl_list marks; // struct utwc_mark
	// Marks are used as "workspaces", and all windows in the current mark are
	// visible
	struct utwc_mark *current_mark;

	struct wl_listener e_new_xdg_surface;

	// Area of the output usable by views.
	struct wlr_box viewspace;
};

/**
 * Each member is a fraction of the viewspace, with the first array index being
 * the numerator and the second being the denominator (E.g. if width[0] was 1
 * and width[1] was 2 then the view width would be half of the viewspace width)
 */
struct utwc_view_rel_box {
	int x[2];
	int y[2];
	int width[2];
	int height[2];
};

// A reference to a mark for an individual output.
struct utwc_view_mark {
	struct utwc_view *view;
	struct utwc_mark *mark;
	struct utwc_view_rel_box box;

	// G_view.marks
	struct wl_list mark_link;
	// Local to each view.
	struct wl_list link;
};

struct utwc_view {
	struct utwc_scene_view *scene_view;

	struct utwc_rule_target rule_target;

	struct utwc_view_mark *current_mark;

	struct wl_list link;
	struct wl_list marks; // struct utwc_view_mark

	struct wl_listener e_xdg_surface_map;
	struct wl_listener e_xdg_surface_unmap;
	struct wl_listener e_xdg_surface_set_app_id;
	struct wl_listener e_xdg_surface_set_title;
	struct wl_listener e_node_destroy;

};

bool utwc_G_desktop_init(void);

void utwc_G_desktop_fini(void);

/**
 * Tell the desktop that an output exists now.
 */
void utwc_desktop_new_output(void);

/**
 * Tell the desktop that an output no longer exists.
 */
void utwc_desktop_destroy_output(void);

/**
 * Should be run when the usable area for views should change.
 */
void utwc_desktop_update_output(void);

void utwc_desktop_goto_mark(char *name);

/**
 * Will goto the next mark in the cycle. `marks` is a NULL-terminated list of
 * mark names to cycle through.
 */
void utwc_desktop_mark_cycle(char **marks, bool reverse);

/**
 * Use utwc_desktop_focus_next for focusing.
 */
struct utwc_view *utwc_desktop_get_next(void);

/**
 * Use utwc_desktop_focus_prev for focusing.
 */
struct utwc_view *utwc_desktop_get_prev(void);

void utwc_desktop_focus_next(void);

void utwc_desktop_focus_prev(void);

void utwc_view_add_mark(struct utwc_view *view, char *name, bool exclusive);

void utwc_view_remove_mark(struct utwc_view *view, char *name);

bool utwc_view_has_mark(struct utwc_view *view, char *name);

void utwc_view_set_box(struct utwc_view *view, char *mark,
		struct utwc_view_rel_box *box);

void utwc_view_set_current_box(struct utwc_view *view,
		struct utwc_view_rel_box *box);

void desktop_update_for_usable_area(void);

void utwc_desktop_clear_focus(void);

void utwc_desktop_focus_any(void);

void utwc_desktop_focus_view(struct utwc_view *view);

extern struct utwc_G_desktop G_desktop;

#endif // UTWC_DESKTOP_H
