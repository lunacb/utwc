#include <wlr/types/wlr_layer_shell_v1.h>
#include "desktop.h"
#include "scene.h"
#include "input/seat.h"
#include "server.h"
#include "util.h"

#include "layers.h"

struct utwc_G_layers G_layers;

struct utwc_scene_node *alias[] = {
		[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND] = NULL,
		[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM] = NULL,
		[ZWLR_LAYER_SHELL_V1_LAYER_TOP] = NULL,
		[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY] = NULL,
};

/**
 * Configure all the children within a spceific layer. `ignore` is used to skip
 * a to-be-unmapped surface.
 */
static inline void configure_node_children(struct utwc_scene_node *node,
		struct wlr_layer_surface_v1 *ignore)
{
	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		struct utwc_layer_surface *utwc_surface = child->data;
		struct utwc_scene_layer_surface_v1 *surface = utwc_surface->scene_surface;
		if(!surface->layer_surface->mapped || surface->layer_surface == ignore)
			continue;

		utwc_scene_layer_surface_v1_configure(surface, &G_layers.full_area,
				&G_layers.usable_area);
	}
}

// Reconfigure all layer-shell surfaces.
static void reconfigure_surfaces(struct wlr_layer_surface_v1 *ignore)
{
	G_layers.usable_area = G_layers.full_area;

	configure_node_children(G_layers.background, ignore);
	configure_node_children(G_layers.bottom, ignore);
	configure_node_children(G_layers.top, ignore);
	configure_node_children(G_layers.overlay, ignore);
}

void utwc_layer_surface_set_configure(struct wlr_box *full_area)
{
	G_layers.configured = true;
	G_layers.full_area = *full_area;
	reconfigure_surfaces(NULL);
}

static bool focus_surface(struct utwc_scene_node *layer,
		struct wlr_layer_surface_v1 *surface)
{
	if(surface->current.keyboard_interactive !=
			ZWLR_LAYER_SURFACE_V1_KEYBOARD_INTERACTIVITY_EXCLUSIVE)
		return false;

	// Set the layer and keyboard focus.
	struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(G_seat.wlr_seat);
	wlr_seat_keyboard_notify_enter(G_seat.wlr_seat,
			surface->surface, keyboard->keycodes,
			keyboard->num_keycodes, &keyboard->modifiers);
	G_seat.layer = layer;

	return true;
}

/**
 * Try to focus a layer-shell surface in a single layer. Return false if no
 * surface was focused.
 */
bool focus_next_node_child(struct utwc_scene_node *layer,
		struct wlr_layer_surface_v1 *ignore)
{
	struct utwc_scene_node *child;
	wl_list_for_each_reverse(child, &layer->state.children, state.link) {
		struct utwc_layer_surface *utwc_surface = child->data;
		struct wlr_layer_surface_v1 *surface =
			utwc_surface->scene_surface->layer_surface;

		if(!surface->mapped || surface == ignore)
			continue;
		if(focus_surface(layer, surface))
			return true;
	}

	return false;
}

/**
 * Focus another layer-shell surface, or tell the desktop to focus a view
 * if an interactable one doesn't exist.
 */
void focus_next_surface(struct wlr_layer_surface_v1 *ignore)
{
	if(focus_next_node_child(G_layers.overlay, ignore))
		return;
	if(focus_next_node_child(G_layers.top, ignore))
		return;
	if(focus_next_node_child(G_layers.bottom, ignore))
		return;
	if(focus_next_node_child(G_layers.background, ignore))
		return;

	// No interactable layer found.
	G_seat.layer = NULL;
	utwc_desktop_focus_any();
}

static void handle_surface_map(struct wl_listener *listener, void *data)
{
	(void)data;

	struct utwc_layer_surface *surface =
		wl_container_of(listener, surface, e_map);
	if(surface->scene_surface->layer_surface->current.keyboard_interactive !=
			ZWLR_LAYER_SURFACE_V1_KEYBOARD_INTERACTIVITY_EXCLUSIVE)
		return;
	utwc_desktop_clear_focus();
	focus_surface(surface->layer, surface->scene_surface->layer_surface);
}

static void handle_surface_unmap(struct wl_listener *listener, void *data)
{
	struct utwc_layer_surface *surface =
		wl_container_of(listener, surface, e_unmap);

	if(G_layers.configured)
		reconfigure_surfaces(data);
	desktop_update_for_usable_area();
	focus_next_surface(data);

	free(surface);
}

static void handle_new_surface(struct wl_listener *listener, void *data)
{
	(void)listener;

	struct wlr_layer_surface_v1 *surface = data;

	struct utwc_layer_surface *utwc_surface = utwc_malloc(sizeof(*utwc_surface));
	HANDLE_SIGNAL(utwc_surface->e_map,
			handle_surface_map,
			surface->events.map);
	HANDLE_SIGNAL(utwc_surface->e_unmap,
			handle_surface_unmap,
			surface->events.unmap);

	utwc_surface->layer = alias[surface->current.layer];

	struct utwc_scene_layer_surface_v1 *scene_surface =
		utwc_scene_layer_surface_v1_create(utwc_surface->layer, surface);

	utwc_surface->scene_surface = scene_surface;
	utwc_surface->rule_target = (struct utwc_rule_target){
		.type = UTWC_RULE_LAYER_SURFACE,
	};

	scene_surface->tree->node.data = utwc_surface;

	if(G_layers.configured) {
		utwc_scene_layer_surface_v1_configure(scene_surface, &G_layers.full_area,
				&G_layers.usable_area);
		desktop_update_for_usable_area();
	}
}

void utwc_layer_surface_unset_configure(void)
{
	G_layers.configured = false;
}

bool utwc_G_layers_init(void)
{
	HANDLE_SIGNAL(G_layers.e_new_surface,
			handle_new_surface,
			G_server.wlr_layer_shell->events.new_surface);

	G_layers.background = &utwc_scene_tree_create(&G_scene.node)->node;
	G_layers.bottom = &utwc_scene_tree_create(&G_scene.node)->node;
	G_layers.view = &utwc_scene_tree_create(&G_scene.node)->node;
	G_layers.top = &utwc_scene_tree_create(&G_scene.node)->node;
	G_layers.overlay = &utwc_scene_tree_create(&G_scene.node)->node;

	alias[ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND] = G_layers.background;
	alias[ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM] = G_layers.bottom;
	alias[ZWLR_LAYER_SHELL_V1_LAYER_TOP] = G_layers.top;
	alias[ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY] = G_layers.overlay;

	G_layers.configured = false;

	return true;
}
