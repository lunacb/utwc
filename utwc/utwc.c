#define _POSIX_C_SOURCE 201112L

#include <stdarg.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>
#include "command.h"
#include "input/binding.h"
#include "input/gesture.h"
#include "input/keybind.h"
#include "log.h"
#include "rule.h"
#include "util.h"

#include "server.h"

static void handle_wlr_log(enum wlr_log_importance importance,
		const char *fmt, va_list args)
{
	char buf[1024];
	snprintf(buf, sizeof(buf), "[wlr] %s", fmt);
	enum utwc_log_verbosity verb;
	switch(importance) {
		case WLR_ERROR:
			verb = UTWC_LOG_ERROR;
			break;
		case WLR_INFO:
			verb = UTWC_LOG_INFO;
			break;
		case WLR_SILENT:
			verb = UTWC_LOG_SILENT;
			break;
		default:
			verb = UTWC_LOG_DEBUG;
	}
	utwc_log_va(verb, NULL, 0, buf, args);
}
static void add_test_bindings()
{
	// *** Next view *** //

	struct utwc_command *command1 = utwc_malloc(sizeof(*command1));
	*command1 = (struct utwc_command){
		.type = UTWC_COMMAND_FOCUS_NEXT,
	};

	struct utwc_gesture_binding binding1 = {
		.start = GESTURE_EDGE_E | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_REL_W,
		.surface_start = GESTURE_ON_ANY,
		.surface_end = GESTURE_ON_ANY,
		.tap = false,
		.fingers = 1,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command1,
			.length = 1,
		}
	};

	// *** Previous view *** //

	struct utwc_command *command2 = utwc_malloc(sizeof(*command2));
	*command2 = (struct utwc_command){
		.type = UTWC_COMMAND_FOCUS_PREV,
	};

	struct utwc_gesture_binding binding2 = {
		.start = GESTURE_EDGE_W | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_REL_E,
		.surface_start = GESTURE_ON_ANY,
		.surface_end = GESTURE_ON_ANY,
		.tap = false,
		.fingers = 1,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command2,
			.length = 1,
		},
	};

	// *** New terminal *** //

	char cmdline_lit3[] = "foot";
	char *cmdline3 = utwc_malloc(sizeof(cmdline_lit3));
	memcpy(cmdline3, cmdline_lit3, sizeof(cmdline_lit3));
	char **argv3 = utwc_malloc(2 * sizeof(*argv3));
	argv3[0] = cmdline3;
	argv3[1] = NULL;
	struct utwc_command *command3 = utwc_malloc(sizeof(*command3));
	*command3 = (struct utwc_command){
		.type = UTWC_COMMAND_EXEC,
		.exec = {
			.cmdline = argv3,
			.has_mark = false,
		},
	};

	struct utwc_gesture_binding binding3 = {
		.start = GESTURE_EDGE_N | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_EDGE_N | GESTURE_EDGE |GESTURE_MIDDLE_EDGE,
		.surface_start = GESTURE_ON_ANY,
		.surface_end = GESTURE_ON_ANY,
		.tap = true,
		.fingers = 1,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command3,
			.length = 1,
		},
	};

	// *** Kill focused window *** //

	struct utwc_rule_item *rule4 = utwc_malloc(sizeof(*rule4));
	*rule4 = (struct utwc_rule_item){
		.type = UTWC_RULE_TYPE_FOCUS,
	};

	struct utwc_command *command4 = utwc_malloc(sizeof(*command4));
	*command4 = (struct utwc_command){
		.type = UTWC_COMMAND_KILL,
		.kill = {
			.specifier = {
				.type = UTWC_TARGET_RULE,
				.rule = {
					.rule = {
						.start = rule4,
						.length = 1,
					},
					.foreach = false,
				}
			},
		}
	};

	struct utwc_gesture_binding binding4 = {
		.start = GESTURE_EDGE_N | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_EDGE_N | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.surface_start = GESTURE_ON_ANY,
		.surface_end = GESTURE_ON_ANY,
		.tap = true,
		.fingers = 2,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command4,
			.length = 1,
		}
	};

	// *** Next workspace *** //

	char **cycle5 = utwc_malloc(10 * sizeof(*cycle5));
	cycle5[0] = utwc_malloc(18);
	for(int i = 0; i < 9; i++) {
		cycle5[i] = &cycle5[0][i*2];
		cycle5[0][i*2] = '0' + i + 1;
		cycle5[0][i*2 + 1] = '\0';
	}
	cycle5[9] = NULL;

	struct utwc_command *command5 = utwc_malloc(sizeof(*command5));
	*command5 = (struct utwc_command){
		.type = UTWC_COMMAND_WORKSPACE_CYCLE,
		.workspace_cycle = {
			.marks = cycle5,
			.reverse = false,
		}
	};

	struct utwc_gesture_binding binding5 = {
		.start = GESTURE_EDGE_E | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_REL_W,
		.surface_start = GESTURE_ON_ANY,
		.surface_end = GESTURE_ON_ANY,
		.tap = false,
		.fingers = 2,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command5,
			.length = 1,
		},
	};

	// *** Previous workspace *** //

	char **cycle6 = utwc_malloc(10 * sizeof(*cycle6));
	cycle6[0] = utwc_malloc(18);
	for(int i = 0; i < 9; i++) {
		cycle6[i] = &cycle6[0][i*2];
		cycle6[0][i*2] = '0' + i + 1;
		cycle6[0][i*2 + 1] = '\0';
	}
	cycle6[9] = NULL;

	struct utwc_command *command6 = utwc_malloc(sizeof(*command6));
	*command6 = (struct utwc_command){
		.type = UTWC_COMMAND_WORKSPACE_CYCLE,
		.workspace_cycle = {
			.marks = cycle6,
			.reverse = true,
		}
	};

	struct utwc_gesture_binding binding6 = {
		.start = GESTURE_EDGE_W | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_REL_E,
		.surface_start = GESTURE_ON_ANY,
		.surface_end = GESTURE_ON_ANY,
		.tap = false,
		.fingers = 2,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command6,
			.length = 1,
		},
	};

	// *** Toggle virtual keyboard *** //

	char cmdline_lit7[] = "/usr/local/bin/toggle-keyboard";
	char *cmdline7 = utwc_malloc(sizeof(cmdline_lit7));
	memcpy(cmdline7, cmdline_lit7, sizeof(cmdline_lit7));
	char **argv7 = utwc_malloc(2 * sizeof(*argv7));
	argv7[0] = cmdline7;
	argv7[1] = NULL;
	struct utwc_command *command7 = utwc_malloc(sizeof(*command7));
	*command7 = (struct utwc_command){
		.type = UTWC_COMMAND_EXEC,
		.exec = {
			.cmdline = argv7,
			.has_mark = false,
		},
	};

	struct utwc_gesture_binding binding7 = {
		.start = GESTURE_EDGE_S | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.end = GESTURE_EDGE_S | GESTURE_EDGE | GESTURE_MIDDLE_EDGE,
		.surface_start = GESTURE_ON_GAPS,
		.surface_end = GESTURE_ON_GAPS,
		.tap = true,
		.fingers = 1,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command7,
			.length = 1,
		},
	};

	// *** New special terminal *** //

	char cmdline_lit8[] = "foot";
	char *cmdline8 = utwc_malloc(sizeof(cmdline_lit8));
	memcpy(cmdline8, cmdline_lit8, sizeof(cmdline_lit8));
	char **argv8 = utwc_malloc(2 * sizeof(*argv8));
	argv8[0] = cmdline8;
	argv8[1] = NULL;
	struct utwc_command *command8 = utwc_malloc(sizeof(*command8));
	*command8 = (struct utwc_command){
		.type = UTWC_COMMAND_EXEC,
		.exec = {
			.cmdline = argv8,
			.has_mark = true,
		},
	};
	strcpy(command8->exec.mark, "9");

	struct utwc_gesture_binding binding8 = {
		.start = GESTURE_CENTER,
		.end = GESTURE_CENTER,
		.surface_start = GESTURE_ON_GAPS,
		.surface_end = GESTURE_ON_GAPS,
		.tap = true,
		.fingers = 1,
		.min_time = { -1, -1 },
		.max_time = { -1, -1 },
		.command = {
			.start = command8,
			.length = 1,
		},
	};

	// *** Main command *** //

	struct utwc_command *main_command =
		utwc_malloc(8 * sizeof(*main_command));
	main_command[0] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding1,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[1] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding2,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[2] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding3,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[3] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding4,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[4] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding5,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[5] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding6,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[6] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding7,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};
	main_command[7] = (struct utwc_command){
		.type = UTWC_COMMAND_BINDGESTURE,
		.bindgesture = {
			.has_start_rule = false,
			.has_end_rule = false,
			.binding = binding8,
			.mode_name = utwc_strdup("normal"),
			.mode_title = NULL,
		},
	};

	struct utwc_command_chain chain = {
		.start = main_command,
		.length = 8,
	};

	utwc_command_chain_execute(&chain);
}

int main(int argc, char **argv)
{
	wlr_log_init(WLR_DEBUG, handle_wlr_log);
	utwc_log_init(UTWC_LOG_DEBUG);

	if(!utwc_server_init()) {
		UTWC_LOG(UTWC_LOG_ERROR, "Failed to start server");
		goto end;
	}

	add_test_bindings();

	//fork & exec into command specified by argv[1].
	if(argc == 2) {
		if(fork() == 0) {
			execl("/bin/sh", "/bin/sh", "-c", argv[1], (void *)NULL);
		}
	}

	utwc_server_run();

	utwc_server_destroy();

end:

	wait(NULL);
}
