#include <sys/wait.h>
#include "command.h"
#include "command.h"
#include "log.h"

#include "pid.h"

void utwc_pid_wait(void)
{
	// TODO: fix all race conditions

	int wstatus;
	pid_t pid;
	while((pid = waitpid(-1, &wstatus, WNOHANG)) > 0) {
		if(!WIFEXITED(wstatus) && !WIFSIGNALED(wstatus))
			continue;

		// Remove the entry for the dead process.
		utwc_pid_mark_pull(pid, NULL);
	}

	if(pid == -1 && errno != ECHILD)
		UTWC_LOG_ERRNO(UTWC_LOG_ERROR, "Failed to waitpid");
}
