#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_xdg_shell.h>
#include "desktop.h"
#include "layers.h"

#include "rule.h"

int nstrcmp(const char *s1, const char *s2)
{
	if(s1 == NULL) s1 = "";
	if(s2 == NULL) s2 = "";
	return strcmp(s1, s2);
}

static bool view_test(struct utwc_rule *rule, struct utwc_view *view)
{
	for(unsigned int i = 0; i < rule->length; i++) {
		struct utwc_rule_item *item = &rule->start[i];
		int res;
		switch(item->type) {
			case UTWC_RULE_TYPE_APP_ID:
				res = nstrcmp(item->app_id,
					view->scene_view->xdg_surface->toplevel->app_id);
				if(res != 0)
					return false;
				break;

			case UTWC_RULE_TYPE_TITLE:
				res = nstrcmp(item->title,
					view->scene_view->xdg_surface->toplevel->title);
				if(res != 0)
					return false;
				break;

			case UTWC_RULE_TYPE_SHELL:
				if(item->shell != UTWC_RULE_SHELL_XDG)
					return false;
				break;

			case UTWC_RULE_TYPE_FOCUS:
				if(G_desktop.activated != view)
					return false;
				break;

			case UTWC_RULE_TYPE_MARK:
				if(!utwc_view_has_mark(view, item->mark))
					return false;
				break;

			case UTWC_RULE_TYPE_PID:;
				pid_t pid;
				wl_client_get_credentials(
					view->scene_view->xdg_surface->client->client,
					&pid, NULL, NULL);
				if(pid != item->pid)
					return false;
				break;
		}
	}

	return true;
}

static bool layer_surface_test(struct utwc_rule *rule,
		struct utwc_layer_surface *surface)
{
	for(unsigned int i = 0; i < rule->length; i++) {
		struct utwc_rule_item *item = &rule->start[i];
		switch(item->type) {
			case UTWC_RULE_TYPE_APP_ID: return false;
			case UTWC_RULE_TYPE_TITLE:  return false;
			case UTWC_RULE_TYPE_FOCUS:  return false;
			case UTWC_RULE_TYPE_MARK:   return false;
			case UTWC_RULE_TYPE_SHELL:
				if(item->shell != UTWC_RULE_SHELL_LAYER)
					return false;
				break;

			case UTWC_RULE_TYPE_PID:;
				pid_t pid;
				struct wl_client *client =
					wl_resource_get_client(surface->scene_surface->
							layer_surface->resource);

				wl_client_get_credentials(client, &pid, NULL, NULL);
				if(pid != item->pid)
					return false;
				break;
		}
	}

	return true;
}

bool utwc_rule_test(struct utwc_rule *rule, struct utwc_rule_target *target)
{
	switch(target->type) {
		case UTWC_RULE_VIEW:;
			struct utwc_view *view =
				wl_container_of(target, view, rule_target);
			return view_test(rule, view);
		case UTWC_RULE_LAYER_SURFACE:;
			struct utwc_layer_surface *surface =
				wl_container_of(target, surface, rule_target);
			return layer_surface_test(rule, surface);
	}

	assert(false && "unreachable");

	return false;
}

struct wl_signal *utwc_rule_target_get_destroy(struct utwc_rule_target *target)
{
	switch(target->type) {
		case UTWC_RULE_VIEW:;
			struct utwc_view *view =
				wl_container_of(target, view, rule_target);
			return &view->scene_view->node.events.destroy;
		case UTWC_RULE_LAYER_SURFACE:;
			struct utwc_layer_surface *surface =
				wl_container_of(target, surface, rule_target);
			return &surface->scene_surface->tree->node.events.destroy;
	}

	assert(false && "unreachable");

	return false;
}

pid_t utwc_rule_target_get_pid(struct utwc_rule_target *target)
{
	struct wl_client *client = NULL;

	switch(target->type) {
		case UTWC_RULE_VIEW:;
			struct utwc_view *view =
				wl_container_of(target, view, rule_target);
			client = view->scene_view->xdg_surface->client->client;
			break;
		case UTWC_RULE_LAYER_SURFACE:;
			struct utwc_layer_surface *surface =
				wl_container_of(target, surface, rule_target);
			struct wl_resource *resource =
				surface->scene_surface->layer_surface->resource;
			client = wl_resource_get_client(resource);
			break;
	}
	pid_t pid;
	wl_client_get_credentials(client, &pid, NULL, NULL);

	return pid;
}

static void utwc_rule_item_fini(struct utwc_rule_item *item)
{
	switch(item->type) {
		case UTWC_RULE_TYPE_APP_ID:
			free(item->app_id);
			break;

		case UTWC_RULE_TYPE_TITLE:
			free(item->title);
			break;

		case UTWC_RULE_TYPE_SHELL: break;
		case UTWC_RULE_TYPE_FOCUS: break;

		case UTWC_RULE_TYPE_MARK:
			free(item->mark);

		case UTWC_RULE_TYPE_PID: break;
	}
}

void utwc_rule_fini(struct utwc_rule *rule)
{
	for(unsigned int i = 0; i < rule->length; i++)
		utwc_rule_item_fini(&rule->start[i]);
	free(rule->start);
}
