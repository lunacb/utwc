#define _POSIX_C_SOURCE 201112L

#include <assert.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "desktop.h"
#include "input/gesture.h"
#include "input/keybind.h"
#include "layers.h"
#include "log.h"
#include "server.h"
#include "util.h"

#include "command.h"

struct utwc_G_command G_command = { .created = false };

typedef void (*command_impl)(struct utwc_command *command);
typedef void (*command_target_impl)(struct utwc_command *command,
		struct utwc_rule_target *target);

static struct utwc_rule_target *toplevel_node_rule_get_one(
		struct utwc_scene_node *node, struct utwc_rule *rule)
{
	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		struct utwc_layer_surface *surface = child->data;
		if(utwc_rule_test(rule, &surface->rule_target))
			return &surface->rule_target;
	}

	return NULL;
}

static bool toplevel_node_rule_foreach(struct utwc_command *command,
		struct utwc_scene_node *node, struct utwc_rule *rule,
		command_target_impl impl, bool foreach)
{
	struct utwc_scene_node *child;
	bool found = false;
	wl_list_for_each(child, &node->state.children, state.link) {
		struct utwc_layer_surface *surface = child->data;
		if(utwc_rule_test(rule, &surface->rule_target)) {
			impl(command, &surface->rule_target);
			found = true;
			if(!foreach)
				return true;
		}
	}

	return found;
}

static struct utwc_rule_target *specifier_get_one(
		struct utwc_target_specifier *specifier, bool views, bool layers)
{
	struct utwc_view *vpos;
	struct utwc_rule_target *target;

	switch(specifier->type) {
		case UTWC_TARGET_WATCHER:;
			struct utwc_rule_index *index =
				wl_container_of(specifier->watcher->matched.prev, index, link);
			target = index->target;
			if((target->type == UTWC_RULE_VIEW && !views) ||
					(target->type == UTWC_RULE_LAYER_SURFACE && !layers))
				return NULL;
			else
				return target;
			break;
		case UTWC_TARGET_RULE:
			if(views) {
				wl_list_for_each(vpos, &G_desktop.views, link)
					if(utwc_rule_test(&specifier->rule.rule,
							&vpos->rule_target))
						return &vpos->rule_target;
			}

			if(layers) {
				if((target = toplevel_node_rule_get_one(G_layers.background,
							&specifier->rule.rule)))
					return target;
				if((target = toplevel_node_rule_get_one(G_layers.bottom,
							&specifier->rule.rule)))
					return target;
				if((target = toplevel_node_rule_get_one(G_layers.top,
							&specifier->rule.rule)))
					return target;
				if((target = toplevel_node_rule_get_one(G_layers.overlay,
							&specifier->rule.rule)))
					return target;
			}

			break;
	}

	return NULL;
}

static void specifier_for_each(
		struct utwc_command *command, struct utwc_target_specifier *specifier,
		command_target_impl impl, bool views, bool layers)
{
	struct utwc_view *vpos;

	switch(specifier->type) {
		case UTWC_TARGET_WATCHER:;
			struct utwc_rule_index *index =
				wl_container_of(specifier->watcher->matched.prev, index, link);
			struct utwc_rule_target *target = index->target;
			if((target->type == UTWC_RULE_VIEW && views) ||
					(target->type == UTWC_RULE_LAYER_SURFACE && layers))
				impl(command, index->target);
			return;
			break;
		case UTWC_TARGET_RULE:;
			bool foreach = specifier->rule.foreach;
			if(views) {
				wl_list_for_each(vpos, &G_desktop.views, link) {
					if(utwc_rule_test(&specifier->rule.rule,
							&vpos->rule_target)) {
						impl(command, &vpos->rule_target);
						if(!foreach)
							return;
					}
				}
			}

			if(layers) {
				if(toplevel_node_rule_foreach(command, G_layers.background,
						&specifier->rule.rule, impl, foreach) && !foreach)
					return;
				if(toplevel_node_rule_foreach(command, G_layers.bottom,
						&specifier->rule.rule, impl, foreach) && !foreach)
					return;
				if(toplevel_node_rule_foreach(command, G_layers.top,
						&specifier->rule.rule, impl, foreach) && !foreach)
					return;
				if(toplevel_node_rule_foreach(command, G_layers.overlay,
						&specifier->rule.rule, impl, foreach) && !foreach)
					return;
				break;
			}
	}
}

static void command_bindgesture(struct utwc_command *command)
{
	struct utwc_rule *start_rule = NULL;
	struct utwc_rule *end_rule = NULL;
	if(command->bindgesture.has_start_rule)
		start_rule = &command->bindgesture.start_rule;
	if(command->bindgesture.has_end_rule)
		end_rule = &command->bindgesture.end_rule;
	struct utwc_binding_mode *mode =
		find_or_create_mode(command->bindgesture.mode_name,
				command->bindgesture.mode_title);

	utwc_gesture_add_binding(&command->bindgesture.binding, mode, start_rule,
			end_rule);

	// Ensure the command isn't run twice.
	assert(command->bindgesture.binding.command.start != NULL);
	command->bindgesture.binding.command.start = NULL;
}

static void command_bindsym(struct utwc_command *command)
{
	struct utwc_binding_mode *mode =
		find_or_create_mode(command->bindsym.mode_name,
				command->bindsym.mode_title);

	utwc_keybind_add_binding(&command->bindsym.binding, mode);

	// Ensure the command isn't run twice.
	assert(command->bindsym.binding.command.start != NULL);
	command->bindsym.binding.command.start = NULL;
}

static void command_exec(struct utwc_command *command)
{
	pid_t pid;
	if((pid = fork()) == 0) {
		execvp(command->exec.cmdline[0], command->exec.cmdline);
		UTWC_LOG_ERRNO(UTWC_LOG_ERROR, "Failed to execvp");
		exit(1);
	}

	if(pid == -1) {
		UTWC_LOG_ERRNO(UTWC_LOG_ERROR, "Failed to fork");
		return;
	}

	if(command->exec.has_mark) {
		struct utwc_pid_mark *mark =
			wl_array_add(&G_command.pid_marks, sizeof(mark));
		*mark = (struct utwc_pid_mark){
			.pid = pid,
		};

		strcpy(mark->name, command->exec.mark);
	}
}

static void command_focus(struct utwc_command *command)
{
	struct utwc_rule_target *target =
		specifier_get_one(&command->focus.specifier, true, false);
	if(target) {
		struct utwc_view *view =
			wl_container_of(target, view, rule_target);
		utwc_desktop_focus_view(view);
	}
}

static void command_focus_next(struct utwc_command *command)
{
	utwc_desktop_focus_next();
}

static void command_focus_prev(struct utwc_command *command)
{
	utwc_desktop_focus_prev();
}

static void command_each_kill(struct utwc_command *command,
		struct utwc_rule_target *target)
{
	(void)command;
	kill(utwc_rule_target_get_pid(target), SIGTERM);
}

static void command_kill(struct utwc_command *command)
{
	specifier_for_each(command, &command->kill.specifier, command_each_kill,
		true, true);
}

static void command_mark(struct utwc_command *command)
{

}

static void command_quit(struct utwc_command *command)
{

}

static void command_resize(struct utwc_command *command)
{

}

static void command_unmark(struct utwc_command *command)
{

}

static void command_watch(struct utwc_command *command)
{

}

static void command_workspace(struct utwc_command *command)
{

}

static void command_workspace_cycle(struct utwc_command *command)
{
	utwc_desktop_mark_cycle(command->workspace_cycle.marks,
			command->workspace_cycle.reverse);
}

static void command_workspace_next(struct utwc_command *command)
{

}

static void command_workspace_prev(struct utwc_command *command)
{

}


static const command_impl command_map[] = {
	[UTWC_COMMAND_BINDGESTURE] = command_bindgesture,
	[UTWC_COMMAND_BINDSYM] = command_bindsym,
	[UTWC_COMMAND_EXEC] = command_exec,
	[UTWC_COMMAND_FOCUS] = command_focus,
	[UTWC_COMMAND_FOCUS_NEXT] = command_focus_next,
	[UTWC_COMMAND_FOCUS_PREV] = command_focus_prev,
	[UTWC_COMMAND_KILL] = command_kill,
	[UTWC_COMMAND_MARK] = command_mark,
	[UTWC_COMMAND_QUIT] = command_quit,
	[UTWC_COMMAND_RESIZE] = command_resize,
	[UTWC_COMMAND_UNMARK] = command_unmark,
	[UTWC_COMMAND_WATCH] = command_watch,
	[UTWC_COMMAND_WORKSPACE] = command_workspace,
	[UTWC_COMMAND_WORKSPACE_CYCLE] = command_workspace_cycle,
	[UTWC_COMMAND_WORKSPACE_NEXT] = command_workspace_next,
	[UTWC_COMMAND_WORKSPACE_PREV] = command_workspace_prev,
};

static void command_execute(struct utwc_command *command)
{
	command_impl func = command_map[command->type];
	func(command);
}

void utwc_command_chain_execute(struct utwc_command_chain *chain)
{
	for(unsigned int i = 0; i < chain->length; i++)
		command_execute(&chain->start[i]);
}

struct utwc_rule_watcher *utwc_watcher_add(struct utwc_rule_watcher *watcher)
{
	struct utwc_rule_watcher *new = utwc_malloc(sizeof(*new));
	*new = *watcher;

	wl_list_init(&new->matched);
	wl_list_init(&new->link);
	wl_list_insert(&G_command.watchers, &new->link);

	return new;
}

void utwc_watcher_fini(struct utwc_rule_watcher *watcher)
{
	struct utwc_rule_index *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &watcher->matched, link) {
		wl_list_remove(&pos->e_destroy.link);
		free(pos);
	}
	utwc_rule_fini(&watcher->rule);
	switch(watcher->executor.type) {
		case UTWC_RULE_EXECUTOR_SINGLE:
			utwc_command_chain_fini(&watcher->executor.single);
			break;
		case UTWC_RULE_EXECUTOR_NONE: break;
	}
	wl_list_remove(&watcher->link);
	free(watcher);
}

static void rule_index_destroy(struct utwc_rule_index *index)
{
	wl_list_remove(&index->link);
	wl_list_remove(&index->e_destroy.link);

	free(index);
}

static void handle_rule_target_destroy(struct wl_listener *listener, void *data)
{
	(void)data;

	struct utwc_rule_index *index =
		wl_container_of(listener, index, e_destroy);

	rule_index_destroy(index);
}

void utwc_watcher_notify_rule_target(struct utwc_rule_target *target)
{
	struct utwc_rule_watcher *pos;
	wl_list_for_each(pos, &G_command.watchers, link) {
		struct utwc_rule_index *ipos;
		// Check if target already matches this rule.
		wl_list_for_each(ipos, &pos->matched, link)
			if(ipos->target == target)
				return;

		if(!utwc_rule_test(&pos->rule, target))
			return;

		struct utwc_rule_index *index = utwc_malloc(sizeof(*index));
		*index = (struct utwc_rule_index){
			.watcher = pos,
			.target = target,
		};

		HANDLE_SIGNAL(index->e_destroy,
				handle_rule_target_destroy,
				*(utwc_rule_target_get_destroy(target)));

		wl_list_insert(&pos->matched, &index->link);

		switch(pos->executor.type) {
			case UTWC_RULE_EXECUTOR_NONE: break;
			case UTWC_RULE_EXECUTOR_SINGLE:
				utwc_command_chain_execute(&pos->executor.single); }
	}
}

bool utwc_pid_mark_pull(pid_t pid, char *buf)
{
	struct utwc_pid_mark *pos;
	wl_array_for_each(pos, &G_command.pid_marks) {
		if(pos->pid == pid) {
			// Give the mark name to the caller.
			if(buf) strcpy(buf, pos->name);

			// Remove the entry.
			G_command.pid_marks.size -= sizeof(*pos);
			struct utwc_pid_mark *last =
				(void *)G_command.pid_marks.data + G_command.pid_marks.size;
			*pos = *last;

			return true;
		}
	}

	return false;
}

bool utwc_G_command_init(void)
{
	G_command.created = true;
	wl_list_init(&G_command.watchers);
	wl_array_init(&G_command.pid_marks);

	return true;
}

static void specifier_fini(struct utwc_target_specifier *specifier)
{
	switch(specifier->type) {
		case UTWC_TARGET_WATCHER: break;
		case UTWC_TARGET_RULE:
			utwc_rule_fini(&specifier->rule.rule);
			break;
	}
}

static void command_fini(struct utwc_command *command)
{
	switch(command->type) {
		case UTWC_COMMAND_BINDGESTURE:
			if(command->bindgesture.has_start_rule)
				utwc_rule_fini(&command->bindgesture.start_rule);
			if(command->bindgesture.has_end_rule)
				utwc_rule_fini(&command->bindgesture.end_rule);
			utwc_command_chain_fini(&command->bindgesture.binding.command);
			free(command->bindgesture.mode_name);
			free(command->bindgesture.mode_title);
			break;

		case UTWC_COMMAND_BINDSYM:
			utwc_command_chain_fini(&command->bindsym.binding.command);
			free(command->bindsym.mode_name);
			free(command->bindsym.mode_title);
			break;

		case UTWC_COMMAND_EXEC:
			free(command->exec.cmdline[0]);
			free(command->exec.cmdline);
			break;

		case UTWC_COMMAND_FOCUS:
			specifier_fini(&command->focus.specifier);
			break;

		case UTWC_COMMAND_FOCUS_NEXT:
			break;

		case UTWC_COMMAND_FOCUS_PREV:
			break;

		case UTWC_COMMAND_KILL:
			specifier_fini(&command->kill.specifier);
			break;

		case UTWC_COMMAND_MARK:
			specifier_fini(&command->mark.specifier);
			free(command->mark.mark);
			break;

		case UTWC_COMMAND_QUIT:
			break;

		case UTWC_COMMAND_RESIZE:
			specifier_fini(&command->resize.specifier);
			free(command->resize.mark);
			break;

		case UTWC_COMMAND_UNMARK:
			specifier_fini(&command->unmark.specifier);
			free(command->unmark.mark);
			break;

		case UTWC_COMMAND_WATCH:
			specifier_fini(&command->watch.specifier);
			utwc_command_chain_fini(&command->watch.command);
			break;

		case UTWC_COMMAND_WORKSPACE:
			free(command->workspace.mark);
			break;

		case UTWC_COMMAND_WORKSPACE_CYCLE:
			free(command->workspace_cycle.marks[0]);
			free(command->workspace_cycle.marks);
			break;

		case UTWC_COMMAND_WORKSPACE_NEXT:
			break;

		case UTWC_COMMAND_WORKSPACE_PREV:
			break;
	}
}

void utwc_command_chain_fini(struct utwc_command_chain *chain)
{
	for(unsigned int i = 0; i < chain->length; i++)
		command_fini(&chain->start[i]);
	free(chain->start);
}

void utwc_G_command_fini(void)
{
	if(!G_command.created)
		return;

	struct utwc_rule_watcher *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &G_command.watchers, link)
		utwc_watcher_fini(pos);
	wl_array_release(&G_command.pid_marks);
}
