#define _POSIX_C_SOURCE 201112L

#include <stddef.h>
#include <time.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_output_damage.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/util/box.h>
#include "desktop.h"
#include "log.h"
#include "scene.h"
#include "server.h"
#include "input/touch.h"
#include "util.h"

#include "output.h"

struct utwc_G_output G_output;

static void handle_damage_frame(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	utwc_scene_output_commit();

	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);
	utwc_scene_output_send_frame_done(&now);
}


//TODO: Use config files, etc. for this.
static inline void configure_mode(struct wlr_output *wlr_output)
{
	if(!wl_list_empty(&wlr_output->modes)) {
		struct wlr_output_mode *mode = wlr_output_preferred_mode(wlr_output);
		wlr_output_set_mode(wlr_output, mode);
	}
}

static void handle_output_destroy(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;

	utwc_scene_output_destroy();
	utwc_desktop_destroy_output();
}

static void handle_output_damage_destroy(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	//TODO: handle
}

static void handle_output_mode(struct wl_listener *listener, void *data)
{
	(void)listener;
	struct wlr_output *wlr_output = data;

	configure_mode(wlr_output);
	if(!wlr_output_commit(wlr_output)) {
		UTWC_LOG(UTWC_LOG_ERROR, "Failed to commit changes for output %s", wlr_output->name);
		return;
	}

	utwc_desktop_update_output();
}

static void handle_new_output(struct wl_listener *listener, void *data)
{
	(void)listener;
	struct wlr_output *const wlr_output = data;

	// If an output is already in use, disable the extra one.
	if(G_output.wlr_output != NULL) {
		UTWC_LOG(UTWC_LOG_INFO, "Disabling extra output %s", wlr_output->name);
		wlr_output_enable(wlr_output, false);
		return;
	}

	wlr_output_init_render(wlr_output, G_server.wlr_allocator,
		G_server.wlr_renderer);

	configure_mode(wlr_output);
	wlr_output_enable(wlr_output, true);
	wlr_output_create_global(wlr_output);

	if(!wlr_output_commit(wlr_output)) {
		UTWC_LOG(UTWC_LOG_ERROR, "Failed to commit changes for output %s", wlr_output->name);
		return;
	}

	utwc_scene_output_create(wlr_output);

	utwc_touch_handle_pending();

	HANDLE_SIGNAL(G_output.e_mode,
			handle_output_mode,
			wlr_output->events.mode)
	HANDLE_SIGNAL(G_output.e_damage_frame,
			handle_damage_frame,
			G_output.damage->events.frame)
	HANDLE_SIGNAL(G_output.e_damage_destroy,
			handle_output_damage_destroy,
			G_output.damage->events.destroy)
	HANDLE_SIGNAL(G_output.e_destroy,
			handle_output_destroy,
			wlr_output->events.destroy)

	utwc_desktop_new_output();

	wlr_output_layout_add(G_server.wlr_output_layout, wlr_output, 0, 0);
}

bool utwc_G_output_init(void)
{
	G_output.wlr_output = NULL;
	G_output.damage = NULL;

	HANDLE_SIGNAL(G_output.e_new_output,
			handle_new_output,
			G_server.wlr_backend->events.new_output)

	return true;
}
