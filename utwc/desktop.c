#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/box.h>
#include "command.h"
#include "layers.h"
#include "output.h"
#include "rule.h"
#include "scene.h"
#include "input/seat.h"
#include "server.h"
#include "util.h"

#include "desktop.h"

#define wl_list_atleast_two_items(list) \
	((list).prev != (list).next)

struct utwc_G_desktop G_desktop = { .created = false };

static inline int rel_box_to_viewspace(int box[2], int length, bool sub)
{
	if(box[0] == 0) {
		return 0;
	} else if(box[0] == box[1]) {
		return length;
	} else {
		int pos = ((long)length) * box[0] / box[1];
		if(sub)
			pos -= 1;
		return pos;
	}
}

/**
 * Generates a viewspace box based on the view's `struct utwc_view_rel_box`. It
 * will be positioned and sized so it works nicely with pseudo-tiling layouts.
 * For example, if two windows had a width of 1/2 of the viewspace and one was
 * at an x of 0 and the other was at an x of 1/2, then they would not overlap
 * or have any gap between them.
 */
static void utwc_view_generate_box(struct utwc_view *view, struct wlr_box *box)
{
	assert(view->current_mark != NULL);
	struct utwc_view_rel_box *vbox = &view->current_mark->box;

	*box = (struct wlr_box){
		.x = rel_box_to_viewspace(vbox->x, G_desktop.viewspace.width,
				false) + G_desktop.viewspace.x,
		.y = rel_box_to_viewspace(vbox->y, G_desktop.viewspace.height,
				false) + G_desktop.viewspace.y,
		.width = rel_box_to_viewspace(vbox->width,
				G_desktop.viewspace.width, true),
		.height = rel_box_to_viewspace(vbox->height,
				G_desktop.viewspace.height, true),
	};
}

/**
 * Called when a view needs to have it's position and size updated.
 */
static void update_view_for_output(struct utwc_view *view)
{
	struct wlr_box box;
	utwc_view_generate_box(view, &box);
	utwc_scene_node_set_position(&view->scene_view->node, box.x, box.y);
	utwc_scene_view_set_size(view->scene_view, box.width, box.height);
}

/**
 * Set `view` to NULL to deactivate any activated view.
 */
static void set_activated_view(struct utwc_view *view)
{
	if(G_desktop.activated) {
		// Deactivate the old one.

		wlr_xdg_toplevel_set_activated(G_desktop.activated->scene_view->xdg_surface,
				false);
		utwc_scene_view_set_border_color(G_desktop.activated->scene_view,
				(float[4]){0.4, 0.4, 0.4, 1.0});
		struct utwc_view *old_activated = G_desktop.activated;
		G_desktop.activated = NULL;

		utwc_watcher_notify_rule_target(&old_activated->rule_target);
	}

	G_desktop.activated = view;

	if(view == NULL)
		return;

	// Check if we're allowed to focus the view.
	if(!seat_can_focus_keyboard(&view->rule_target))
		return;

	utwc_scene_view_set_border_color(view->scene_view,
			(float[4]){1.0, 0.0, 0.0, 1.0});

	wlr_xdg_toplevel_set_activated(view->scene_view->xdg_surface, true);
	struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(G_seat.wlr_seat);
	wlr_seat_keyboard_notify_enter(G_seat.wlr_seat,
			view->scene_view->xdg_surface->surface, keyboard->keycodes,
			keyboard->num_keycodes, &keyboard->modifiers);

	utwc_watcher_notify_rule_target(&G_desktop.activated->rule_target);
}

/**
 * Called when no view is active because the active one was destroyed or a new
 * set of views are shown.
 */
static void set_last_view_active(void)
{
	if(G_desktop.current_mark &&
			G_desktop.current_mark->views.prev != &G_desktop.current_mark->views) {
		struct utwc_view_mark *lastview =
			wl_container_of(G_desktop.current_mark->views.prev, lastview, mark_link);
		set_activated_view(lastview->view);
	} else {
		G_desktop.activated = NULL;
	}
}

void utwc_desktop_clear_focus(void)
{
	set_activated_view(NULL);
}

void utwc_desktop_focus_any(void)
{
	set_last_view_active();
}

static struct utwc_mark *find_or_create_mark(char *name)
{
	assert(strlen(name) < MARK_NAME_MAX);

	struct utwc_mark *pos;
	wl_list_for_each(pos, &G_desktop.marks, link) {
		// Return a matching mark if it already exists.
		if(!strcmp(pos->name, name))
			return pos;
	}

	// No marks exist, create a new one.

	struct utwc_mark *mark = utwc_malloc(sizeof(struct utwc_mark));
	wl_list_init(&mark->views);
	wl_list_insert(&G_desktop.marks, &mark->link);

	strcpy(mark->name, name);

	return mark;
}

void utwc_desktop_goto_mark(char *name)
{
	struct utwc_view_mark *pos;
	if(G_desktop.current_mark != NULL) {
		// Hide the views in the old mark.
		set_activated_view(NULL);
		wl_list_for_each(pos, &G_desktop.current_mark->views, mark_link) {
			utwc_scene_node_set_enabled(&pos->view->scene_view->node, false);
			pos->view->current_mark = NULL;
		}
	}

	G_desktop.current_mark = find_or_create_mark(name);

	// Show the views in the new mark.
	struct utwc_scene_node *prev = NULL;
	wl_list_for_each(pos, &G_desktop.current_mark->views, mark_link) {
		pos->view->current_mark = pos;
		struct utwc_scene_node *node = &pos->view->scene_view->node;
		// Visually order the nodes corretly.
		if(prev != NULL)
			utwc_scene_node_place_above(node, prev);
		utwc_scene_node_set_enabled(&pos->view->scene_view->node, true);
		prev = node;
	}

	if(G_output.wlr_output != NULL) {
		wl_list_for_each(pos, &G_desktop.current_mark->views, mark_link)
			update_view_for_output(pos->view);

		// If the mark contains any views then make the last one activated.
		if(&G_desktop.current_mark->views != G_desktop.current_mark->views.prev) {
			struct utwc_view_mark *mark =
				wl_container_of(G_desktop.current_mark->views.prev, mark, mark_link);
			set_activated_view(mark->view);
		}
	}
}

void utwc_desktop_mark_cycle(char **marks, bool reverse)
{
	// No elements in the cycle.
	if(marks[0] == NULL)
		return;

	if(marks[1] == NULL) {
		// Special case of only a single element in the cycle.
		if(G_desktop.current_mark == NULL ||
				strcmp(marks[0], G_desktop.current_mark->name) != 0)
			utwc_desktop_goto_mark(marks[1]);
		return;
	}

	char *name = NULL;
	if(reverse) {
		// Find the index of the last element.
		int last;
		for(last = 2; marks[last] != NULL; last++)
			;
		// Iterate backwards.
		for(int i = --last; i >= 0; i--) {
			if(G_desktop.current_mark != NULL &&
					!strcmp(marks[i], G_desktop.current_mark->name)) {
				if(i == 0)
					name = marks[last];
				else
					name = marks[i-1];
			}
		}
		if(name == NULL)
			name = marks[last];
	} else {
		// Iterate forwards.
		for(int i = 0; marks[i] != NULL; i++) {
			if(G_desktop.current_mark != NULL &&
					!strcmp(marks[i], G_desktop.current_mark->name)) {
				// If marks[i+1] is null then this will be changed later.
				name = marks[i+1];
			}
		}
		if(name == NULL)
			name = marks[0];
	}

	utwc_desktop_goto_mark(name);
}

void utwc_desktop_focus_view(struct utwc_view *view)
{
	struct utwc_view_mark *mark = NULL;
	struct utwc_view_mark *pos;

	wl_list_for_each(pos, &view->marks, link) {
		if(pos->mark == G_desktop.current_mark) {
			mark = pos;
			break;
		}
	}

	if(mark == NULL)
		return;

	// Move the selected view to the top.
	wl_list_remove(&mark->mark_link);
	wl_list_insert(G_desktop.current_mark->views.prev, &mark->mark_link);

	// Do the same for the scene node.
	struct utwc_scene_node *sibling =
		wl_container_of(G_layers.view->state.children.prev, sibling, state.link);
	utwc_scene_node_place_above(&mark->view->scene_view->node, sibling);

	set_last_view_active();
}

struct utwc_view *utwc_desktop_get_next(void)
{
	if(G_desktop.current_mark == NULL ||
			!wl_list_atleast_two_items(G_desktop.current_mark->views))
		return NULL;

	struct wl_list *list = G_desktop.current_mark->views.prev;
	struct utwc_view_mark *mark = wl_container_of(list, mark, mark_link);
	return mark->view;
}

struct utwc_view *utwc_desktop_get_prev(void)
{
	if(G_desktop.current_mark == NULL ||
			!wl_list_atleast_two_items(G_desktop.current_mark->views))
		return NULL;

	struct wl_list *list = G_desktop.current_mark->views.next;
	struct utwc_view_mark *mark = wl_container_of(list, mark, mark_link);
	return mark->view;
}

void utwc_desktop_focus_next(void)
{
	if(G_desktop.current_mark == NULL ||
			!wl_list_atleast_two_items(G_desktop.current_mark->views))
		return;

	struct wl_list *list = G_desktop.current_mark->views.prev;
	struct utwc_view_mark *mark = wl_container_of(list, mark, mark_link);
	// Move the topmost view to the bottom.
	wl_list_remove(list);
	wl_list_insert(&G_desktop.current_mark->views, list);
	// Do the same for the scene node.

	struct utwc_scene_node *sibling =
		wl_container_of(G_layers.view->state.children.next, sibling, state.link);
	utwc_scene_node_place_below(&mark->view->scene_view->node, sibling);

	set_last_view_active();
}

void utwc_desktop_focus_prev(void)
{
	if(G_desktop.current_mark == NULL ||
			!wl_list_atleast_two_items(G_desktop.current_mark->views))
		return;

	struct wl_list *list = G_desktop.current_mark->views.next;
	struct utwc_view_mark *mark = wl_container_of(list, mark, mark_link);
	// Move the bottommost view to the top.
	wl_list_remove(list);
	wl_list_insert(G_desktop.current_mark->views.prev, list);
	// Do the same for the scene node.
	struct utwc_scene_node *sibling =
		wl_container_of(G_layers.view->state.children.prev, sibling, state.link);
	utwc_scene_node_place_above(&mark->view->scene_view->node, sibling);

	set_last_view_active();
}

static void handle_xdg_surface_unmap(struct wl_listener *listener, void *data)
{
	(void)data;

	struct utwc_view *view =
		wl_container_of(listener, view, e_xdg_surface_unmap);

	(void)view;

	wl_list_remove(&view->link);
	struct utwc_view_mark *pos;
	struct utwc_view_mark *tmp;
	wl_list_for_each_safe(pos, tmp, &view->marks, link) {
		wl_list_remove(&pos->link);
		wl_list_remove(&pos->mark_link);
		free(pos);
	}

	if(G_desktop.activated == view)
		set_last_view_active();
}

static void handle_node_destroy(struct wl_listener *listener, void *data)
{
	(void)data;

	struct utwc_view *view =
		wl_container_of(listener, view, e_node_destroy);

	wl_list_remove(&view->e_xdg_surface_map.link);
	wl_list_remove(&view->e_xdg_surface_unmap.link);
	wl_list_remove(&view->e_xdg_surface_set_app_id.link);
	wl_list_remove(&view->e_xdg_surface_set_title.link);
	wl_list_remove(&view->e_node_destroy.link);

	free(view);

	(void)view;
}

bool utwc_view_has_mark(struct utwc_view *view, char *name)
{
	assert(strlen(name) < MARK_NAME_MAX);

	struct utwc_mark *mark = NULL;

	{
		struct utwc_mark *pos;
		wl_list_for_each(pos, &G_desktop.marks, link) {
			if(!strcmp(pos->name, name)) {
				mark = pos;
				break;
			}
		}
	}

	if(mark == NULL)
		return false;

	struct utwc_view_mark *pos;
	wl_list_for_each(pos, &view->marks, link)
		if(pos->mark == mark)
			return true;

	return false;
}

/**
 * Unsets the activated view if needed.
 */
static void view_remove_mark(struct utwc_view_mark *mark)
{
	wl_list_remove(&mark->mark_link);
	wl_list_remove(&mark->link);
	if(mark->view->current_mark == mark) {
		mark->view->current_mark = NULL;
		if(mark->view == G_desktop.activated)
			set_activated_view(NULL);
		utwc_scene_node_set_enabled(&mark->view->scene_view->node, false);
	}
}

static void view_add_mark(struct utwc_view *view, struct utwc_mark *mark,
		bool exclusive)
{
	bool marked = false;
	struct utwc_view_mark *pos;
	wl_list_for_each(pos, &mark->views, link) {
		if(pos->view == view) {
			marked = true;
		} else if(exclusive) {
			view_remove_mark(pos);
		}
	}

	if(marked) {
		// Activate the view if it's exclusively marked and can be activated.
		if(exclusive && mark == G_desktop.current_mark &&
				G_output.wlr_output != NULL)
			set_activated_view(view);
		return;
	}

	struct utwc_view_mark *new_mark = utwc_malloc(sizeof(struct utwc_view_mark));

	*new_mark = (struct utwc_view_mark){
		.view = view,
		.mark = mark,
		.box = {
			.x = {0, 1},
			.y = {0, 1},
			.width = {1, 1},
			.height = {1, 1},
		}
	};

	wl_list_insert(mark->views.prev, &new_mark->mark_link);
	wl_list_insert(&view->marks, &new_mark->link);

	// If adding on the current mark, show and focus the window.
	if(mark == G_desktop.current_mark) {
		view->current_mark = new_mark;
		utwc_scene_node_raise_to_top(&view->scene_view->node);
		utwc_scene_node_set_enabled(&view->scene_view->node, true);

		if(G_output.wlr_output != NULL) {
			update_view_for_output(view);
			set_activated_view(view);
		}
	}

	utwc_watcher_notify_rule_target(&view->rule_target);
}

void utwc_view_add_mark(struct utwc_view *view, char *name, bool exclusive)
{
	view_add_mark(view, find_or_create_mark(name), exclusive);
}

void utwc_view_remove_mark(struct utwc_view *view, char *name)
{
	struct utwc_view_mark *pos;
	wl_list_for_each(pos, &view->marks, link) {
		if(!strcmp(pos->mark->name, name)) {
			view_remove_mark(pos);
			if(G_desktop.activated == NULL)
				set_last_view_active();
			return;
		}
	}
}

void utwc_view_set_current_box(struct utwc_view *view,
		struct utwc_view_rel_box *box)
{
	if(view->current_mark == NULL)
		return;
	view->current_mark->box = *box;
	if(G_output.wlr_output != NULL && view->scene_view->node.state.enabled)
		update_view_for_output(view);
}

void utwc_view_set_box(struct utwc_view *view, char *mark,
		struct utwc_view_rel_box *box)
{
	assert(strlen(mark) < MARK_NAME_MAX);

	struct utwc_view_mark *pos;
	wl_list_for_each(pos, &view->marks, link) {
		if(!strcmp(pos->mark->name, mark)) {
			pos->box = *box;
			if(G_output.wlr_output != NULL &&
					view->scene_view->node.state.enabled)
				update_view_for_output(view);
		}
	}
}

static void handle_xdg_surface_map(struct wl_listener *listener, void *data)
{
	(void)data;

	struct utwc_view *view =
		wl_container_of(listener, view, e_xdg_surface_map);

	wl_list_insert(&G_desktop.views, &view->link);
	if(G_desktop.current_mark)
		view_add_mark(view, G_desktop.current_mark, false);

	// TODO: put this in a function
	pid_t pid;
	wl_client_get_credentials(view->scene_view->xdg_surface->client->client,
		&pid, NULL, NULL);

	char pid_mark[MARK_NAME_MAX];
	if(utwc_pid_mark_pull(pid, pid_mark))
		utwc_view_add_mark(view, pid_mark, false);

}

static void handle_xdg_surface_set_app_id(struct wl_listener *listener,
		void *data)
{
	(void)data;
	struct utwc_view *view =
		wl_container_of(listener, view, e_xdg_surface_set_app_id);

	utwc_watcher_notify_rule_target(&view->rule_target);
}

static void handle_xdg_surface_set_title(struct wl_listener *listener,
		void *data)
{
	(void)data;
	struct utwc_view *view =
		wl_container_of(listener, view, e_xdg_surface_set_title);

	utwc_watcher_notify_rule_target(&view->rule_target);
}

static void handle_new_xdg_surface(struct wl_listener *listener, void *data)
{
	(void)listener;
	struct wlr_xdg_surface *xdg_surface = data;

	if(xdg_surface->role == WLR_XDG_SURFACE_ROLE_POPUP) {
		struct wlr_xdg_surface *parent =
			wlr_xdg_surface_from_wlr_surface(xdg_surface->popup->parent);
		struct utwc_scene_node *parent_node = parent->data;
		xdg_surface->data = utwc_scene_xdg_surface_create(parent_node,
			xdg_surface);

		return;
	}

	// Surface is a toplevel, create a view for it.

	struct utwc_view *view = utwc_malloc(sizeof(struct utwc_view));

	view->rule_target = (struct utwc_rule_target){
		.type = UTWC_RULE_VIEW,
	};

	view->current_mark = NULL;

	wl_list_init(&view->link);
	wl_list_init(&view->marks);

	struct utwc_scene_view *scene_view =
		utwc_scene_view_create(G_layers.view, xdg_surface);
	view->scene_view = scene_view;

	scene_view->node.data = view;

	xdg_surface->data = &scene_view->node;
	unsigned int width[4] = {2, 2, 2, 2};
	utwc_scene_view_set_border_width(scene_view, width);
	utwc_scene_view_set_border_color(scene_view, (float[4]){0.4, 0.4, 0.4, 1.0});

	HANDLE_SIGNAL(view->e_xdg_surface_unmap,
			handle_xdg_surface_unmap,
			xdg_surface->events.unmap);

	HANDLE_SIGNAL(view->e_xdg_surface_map,
			handle_xdg_surface_map,
			xdg_surface->events.map);

	HANDLE_SIGNAL(view->e_xdg_surface_set_app_id,
			handle_xdg_surface_set_app_id,
			xdg_surface->toplevel->events.set_app_id);

	HANDLE_SIGNAL(view->e_xdg_surface_set_title,
			handle_xdg_surface_set_title,
			xdg_surface->toplevel->events.set_title);

	HANDLE_SIGNAL(view->e_node_destroy,
			handle_node_destroy,
			scene_view->node.events.destroy);

	utwc_watcher_notify_rule_target(&view->rule_target);
}

/**
 * Called when an output's size changes or a layer-shell surface changes the
 * usable area.
 */
void desktop_update_for_usable_area(void)
{
	const int border = 32;
	G_desktop.viewspace = (struct wlr_box){
		.x = G_layers.usable_area.x + border,
		.y = G_layers.usable_area.y + border,
		.width = G_layers.usable_area.width - border*2,
		.height = G_layers.usable_area.height - border*2,
	};

	struct utwc_view_mark *pos;
	if(G_desktop.current_mark) {
		wl_list_for_each(pos, &G_desktop.current_mark->views, mark_link) {
			update_view_for_output(pos->view);
		}
	}
}

void utwc_desktop_update_output(void)
{
	struct wlr_box area = {
		.x = 0,
		.y = 0,
	};
	wlr_output_effective_resolution(G_output.wlr_output, &area.width,
			&area.height);

	utwc_layer_surface_set_configure(&area);
	desktop_update_for_usable_area();
}

void utwc_desktop_new_output(void)
{
	utwc_desktop_update_output();
	set_last_view_active();
}

void utwc_desktop_destroy_output(void)
{
	set_activated_view(NULL);
	wlr_seat_keyboard_clear_focus(G_seat.wlr_seat);
	utwc_layer_surface_unset_configure();
}

bool utwc_G_desktop_init(void)
{
	G_desktop.created = true;
	wl_list_init(&G_desktop.views);
	wl_list_init(&G_desktop.marks);
	G_desktop.current_mark = NULL;

	G_desktop.activated = NULL;

	G_desktop.viewspace = (struct wlr_box){0,0,0,0};

	HANDLE_SIGNAL(G_desktop.e_new_xdg_surface,
			handle_new_xdg_surface,
			G_server.wlr_xdg_shell->events.new_surface);

	utwc_desktop_goto_mark("1");

	return true;
}

void utwc_G_desktop_fini(void)
{
	if(!G_desktop.created)
		return;

	free_list(&G_desktop.marks, link, struct utwc_mark);
	struct utwc_view *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &G_desktop.views, link) {
		free_list(&pos->marks, link, struct utwc_view_mark);
		free(pos);
	}
}
