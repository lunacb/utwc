#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output_damage.h>
#include <wlr/types/wlr_presentation_time.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>
#include <wlr/util/region.h>
#include "log.h"
#include "output.h"
#include "input/seat.h"
#include "server.h"

#include "scene.h"

struct utwc_G_scene G_scene = { .created = false };

// TODO: don't do this. wlr_signal_emit_safe is a function for internal use
// within wlroots
void wlr_signal_emit_safe(struct wl_signal *signal, void *data);

static struct utwc_scene_tree *scene_tree_from_node(struct utwc_scene_node *node)
{
	assert(node->type == UTWC_SCENE_NODE_TREE);
	return (struct utwc_scene_tree *)node;
}

struct utwc_scene_surface *utwc_scene_surface_from_node(
		struct utwc_scene_node *node)
{
	assert(node->type == UTWC_SCENE_NODE_SURFACE);
	return (struct utwc_scene_surface *)node;
}

static struct utwc_scene_view *scene_view_from_node(
		struct utwc_scene_node *node)
{
	assert(node->type == UTWC_SCENE_NODE_VIEW);
	return (struct utwc_scene_view *)node;
}

static void scene_node_state_init(struct utwc_scene_node_state *state)
{
	wl_list_init(&state->children);
	wl_list_init(&state->link);
	state->enabled = true;
	state->inherit_crop = true;
	state->x = 0;
	state->y = 0;
}

static void scene_node_state_finish(struct utwc_scene_node_state *state)
{
	wl_list_remove(&state->link);
}

static void scene_node_init(struct utwc_scene_node *node,
		enum utwc_scene_node_type type, struct utwc_scene_node *parent)
{
	assert(type == UTWC_SCENE_NODE_ROOT || parent != NULL);

	node->type = type;
	node->parent = parent;
	scene_node_state_init(&node->state);
	wl_signal_init(&node->events.destroy);

	if (parent != NULL) {
		wl_list_insert(parent->state.children.prev, &node->state.link);
	}
}

static void scene_node_finish(struct utwc_scene_node *node)
{
	wlr_signal_emit_safe(&node->events.destroy, NULL);

	struct utwc_scene_node *child, *child_tmp;
	wl_list_for_each_safe(child, child_tmp,
			&node->state.children, state.link) {
		utwc_scene_node_destroy(child);
	}

	scene_node_state_finish(&node->state);
}

static void scene_node_damage_whole(struct utwc_scene_node *node);

void utwc_scene_node_destroy(struct utwc_scene_node *node)
{
	if (node == NULL) {
		return;
	}

	scene_node_damage_whole(node);
	scene_node_finish(node);

	switch (node->type) {
	case UTWC_SCENE_NODE_ROOT:;
		utwc_scene_output_destroy();
		break;
	case UTWC_SCENE_NODE_TREE:;
		struct utwc_scene_tree *tree = scene_tree_from_node(node);
		free(tree);
		break;
	case UTWC_SCENE_NODE_SURFACE:;
		struct utwc_scene_surface *scene_surface = utwc_scene_surface_from_node(node);

		// This is a noop if wlr_surface_send_enter() wasn't previously called for
		// the given output.
		if(G_output.wlr_output)
			wlr_surface_send_leave(scene_surface->surface, G_output.wlr_output);

		wl_list_remove(&scene_surface->surface_commit.link);
		wl_list_remove(&scene_surface->surface_destroy.link);

		free(scene_surface);
		break;
	case UTWC_SCENE_NODE_VIEW:;
		struct utwc_scene_view *scene_view = scene_view_from_node(node);
		wl_list_remove(&scene_view->xdg_surface_destroy.link);
		wl_list_remove(&scene_view->xdg_surface_map.link);
		wl_list_remove(&scene_view->xdg_surface_unmap.link);
		wl_list_remove(&scene_view->xdg_surface_commit.link);
		free(scene_view);
		break;
	}
}

bool utwc_G_scene_init(void)
{
	G_scene.created = true;
	scene_node_init(&G_scene.node, UTWC_SCENE_NODE_ROOT, NULL);
	return true;
}

struct utwc_scene_tree *utwc_scene_tree_create(struct utwc_scene_node *parent)
{
	struct utwc_scene_tree *tree = calloc(1, sizeof(struct utwc_scene_tree));
	if (tree == NULL) {
		return NULL;
	}
	scene_node_init(&tree->node, UTWC_SCENE_NODE_TREE, parent);

	return tree;
}

static void scene_surface_handle_surface_destroy(struct wl_listener *listener,
		void *data)
{
	(void)data;

	struct utwc_scene_surface *scene_surface =
		wl_container_of(listener, scene_surface, surface_destroy);
	utwc_scene_node_destroy(&scene_surface->node);
}

static void scene_surface_handle_surface_commit(struct wl_listener *listener,
		void *data)
{
	(void)data;

	struct utwc_scene_surface *scene_surface =
		wl_container_of(listener, scene_surface, surface_commit);
	struct wlr_surface *surface = scene_surface->surface;

	int lx, ly;
	bool enabled = utwc_scene_node_coords(&scene_surface->node, &lx, &ly);

	if (surface->current.width != scene_surface->prev_width ||
			surface->current.height != scene_surface->prev_height) {
		scene_surface->prev_width = surface->current.width;
		scene_surface->prev_height = surface->current.height;
	}

	if (!enabled) {
		return;
	}

	// Even if the surface hasn't submitted damage, schedule a new frame if
	// the client has requested a wl_surface.frame callback.
	if (!wl_list_empty(&surface->current.frame_callback_list) &&
			G_output.wlr_output != NULL) {
		wlr_output_schedule_frame(G_output.wlr_output);
	}

	if (!pixman_region32_not_empty(&surface->buffer_damage)) {
		return;
	}

	if(G_output.wlr_output) {
		pixman_region32_t damage;
		pixman_region32_init(&damage);
		wlr_surface_get_effective_damage(surface, &damage);

		pixman_region32_translate(&damage,
			lx, ly);

		wlr_region_scale(&damage, &damage, G_output.wlr_output->scale);
		if (ceil(G_output.wlr_output->scale) > surface->current.scale) {
			// When scaling up a surface it'll become blurry, so we need to
			// expand the damage region.
			wlr_region_expand(&damage, &damage,
				ceil(G_output.wlr_output->scale) - surface->current.scale);
		}
		wlr_output_damage_add(G_output.damage, &damage);
		pixman_region32_fini(&damage);
	}
}

struct utwc_scene_surface *utwc_scene_surface_create(struct utwc_scene_node *parent,
		struct wlr_surface *surface)
{
	struct utwc_scene_surface *scene_surface =
		calloc(1, sizeof(struct utwc_scene_surface));
	if (scene_surface == NULL) {
		return NULL;
	}
	scene_node_init(&scene_surface->node, UTWC_SCENE_NODE_SURFACE, parent);

	scene_surface->surface = surface;

	scene_surface->surface_destroy.notify = scene_surface_handle_surface_destroy;
	wl_signal_add(&surface->events.destroy, &scene_surface->surface_destroy);

	scene_surface->surface_commit.notify = scene_surface_handle_surface_commit;
	wl_signal_add(&surface->events.commit, &scene_surface->surface_commit);

	scene_node_damage_whole(&scene_surface->node);

	if(G_output.wlr_output)
		wlr_surface_send_enter(scene_surface->surface, G_output.wlr_output);

	return scene_surface;
}

static void scene_node_get_size(struct utwc_scene_node *node,
		int *width, int *height)
{
	*width = 0;
	*height = 0;

	switch (node->type) {
	case UTWC_SCENE_NODE_ROOT:
	case UTWC_SCENE_NODE_TREE:
		return;
	case UTWC_SCENE_NODE_SURFACE:;
		struct utwc_scene_surface *scene_surface =
			utwc_scene_surface_from_node(node);
		*width = scene_surface->surface->current.width;
		*height = scene_surface->surface->current.height;
		break;
	case UTWC_SCENE_NODE_VIEW:;
		struct utwc_scene_view *scene_view = scene_view_from_node(node);
		*width = scene_view->real_width;
		*height = scene_view->real_height;
	}
}

static int scale_length(int length, int offset, float scale)
{
	return round((offset + length) * scale) - round(offset * scale);
}

static void scale_box(struct wlr_box *box, float scale)
{
	box->width = scale_length(box->width, box->x, scale);
	box->height = scale_length(box->height, box->y, scale);
	box->x = round(box->x * scale);
	box->y = round(box->y * scale);
}

static void _scene_node_damage_whole(struct utwc_scene_node *node,
		int lx, int ly)
{
	if (!node->state.enabled) {
		return;
	}

	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		_scene_node_damage_whole(child,
			lx + child->state.x, ly + child->state.y);
	}

	int width, height;
	scene_node_get_size(node, &width, &height);

	if(G_output.wlr_output) {
		struct wlr_box box = {
			.x = lx,
			.y = ly,
			.width = width,
			.height = height,
		};

		scale_box(&box, G_output.wlr_output->scale);

		wlr_output_damage_add_box(G_output.damage, &box);
	}
}

static void scene_node_damage_whole(struct utwc_scene_node *node)
{
	if(!G_output.wlr_output)
		return;

	int lx, ly;
	if (!utwc_scene_node_coords(node, &lx, &ly)) {
		return;
	}

	_scene_node_damage_whole(node, lx, ly);
}

void utwc_scene_node_set_enabled(struct utwc_scene_node *node, bool enabled)
{
	if (node->state.enabled == enabled) {
		return;
	}

	// One of these damage_whole() calls will short-circuit and be a no-op
	scene_node_damage_whole(node);
	node->state.enabled = enabled;
	scene_node_damage_whole(node);
}

void utwc_scene_node_set_inherit_crop(struct utwc_scene_node *node,
		bool inherit_crop)
{
	if(node->state.inherit_crop == inherit_crop) {
		return;
	}

	node->state.inherit_crop = inherit_crop;
	scene_node_damage_whole(node);
}

void utwc_scene_node_set_position(struct utwc_scene_node *node, int x, int y)
{
	if (node->state.x == x && node->state.y == y) {
		return;
	}

	scene_node_damage_whole(node);
	node->state.x = x;
	node->state.y = y;
	scene_node_damage_whole(node);
}

void utwc_scene_node_place_above(struct utwc_scene_node *node,
		struct utwc_scene_node *sibling)
{
	assert(node != sibling);
	assert(node->parent == sibling->parent);

	if (node->state.link.prev == &sibling->state.link) {
		return;
	}

	wl_list_remove(&node->state.link);
	wl_list_insert(&sibling->state.link, &node->state.link);

	scene_node_damage_whole(node);
	scene_node_damage_whole(sibling);
}

void utwc_scene_node_place_below(struct utwc_scene_node *node,
		struct utwc_scene_node *sibling)
{
	assert(node != sibling);
	assert(node->parent == sibling->parent);

	if (node->state.link.next == &sibling->state.link) {
		return;
	}

	wl_list_remove(&node->state.link);
	wl_list_insert(sibling->state.link.prev, &node->state.link);

	scene_node_damage_whole(node);
	scene_node_damage_whole(sibling);
}

void utwc_scene_node_raise_to_top(struct utwc_scene_node *node)
{
	struct utwc_scene_node *current_top = wl_container_of(
		node->parent->state.children.prev, current_top, state.link);
	if (node == current_top) {
		return;
	}
	utwc_scene_node_place_above(node, current_top);
}

void utwc_scene_node_lower_to_bottom(struct utwc_scene_node *node)
{
	struct utwc_scene_node *current_bottom = wl_container_of(
		node->parent->state.children.next, current_bottom, state.link);
	if (node == current_bottom) {
		return;
	}
	utwc_scene_node_place_below(node, current_bottom);
}

void utwc_scene_node_reparent(struct utwc_scene_node *node,
		struct utwc_scene_node *new_parent)
{
	assert(node->type != UTWC_SCENE_NODE_ROOT && new_parent != NULL);

	if (node->parent == new_parent) {
		return;
	}

	/* Ensure that a node cannot become its own ancestor */
	for (struct utwc_scene_node *ancestor = new_parent; ancestor != NULL;
			ancestor = ancestor->parent) {
		assert(ancestor != node);
	}

	scene_node_damage_whole(node);

	wl_list_remove(&node->state.link);
	node->parent = new_parent;
	wl_list_insert(new_parent->state.children.prev, &node->state.link);

	scene_node_damage_whole(node);
}

bool utwc_scene_node_coords(struct utwc_scene_node *node,
		int *lx_ptr, int *ly_ptr)
{
	int lx = 0, ly = 0;
	bool enabled = true;
	while (node != NULL) {
		lx += node->state.x;
		ly += node->state.y;
		enabled = enabled && node->state.enabled;
		node = node->parent;
	}

	*lx_ptr = lx;
	*ly_ptr = ly;
	return enabled;
}

static void scene_node_for_each_surface(struct utwc_scene_node *node,
		int lx, int ly, wlr_surface_iterator_func_t user_iterator,
		void *user_data)
{
	if (!node->state.enabled) {
		return;
	}

	lx += node->state.x;
	ly += node->state.y;

	if (node->type == UTWC_SCENE_NODE_SURFACE) {
		struct utwc_scene_surface *scene_surface = utwc_scene_surface_from_node(node);
		user_iterator(scene_surface->surface, lx, ly, user_data);
	}

	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		scene_node_for_each_surface(child, lx, ly, user_iterator, user_data);
	}
}

void utwc_scene_node_for_each_surface(struct utwc_scene_node *node,
		wlr_surface_iterator_func_t user_iterator, void *user_data)
{
	scene_node_for_each_surface(node, 0, 0, user_iterator, user_data);
}

struct utwc_scene_node *utwc_scene_node_at(struct utwc_scene_node *node,
		double lx, double ly, double *nx, double *ny)
{
	if (!node->state.enabled) {
		return NULL;
	}

	lx -= node->state.x;
	ly -= node->state.y;

	struct utwc_scene_node *child;
	wl_list_for_each_reverse(child, &node->state.children, state.link) {
		struct utwc_scene_node *node =
			utwc_scene_node_at(child, lx, ly, nx, ny);
		if (node != NULL) {
			return node;
		}
	}

	bool intersects = false;
	switch (node->type) {
	case UTWC_SCENE_NODE_ROOT:
	case UTWC_SCENE_NODE_TREE:
		break;
	case UTWC_SCENE_NODE_SURFACE:;
		struct utwc_scene_surface *scene_surface = utwc_scene_surface_from_node(node);
		intersects = wlr_surface_point_accepts_input(scene_surface->surface, lx, ly);
		break;
	case UTWC_SCENE_NODE_VIEW:;
		int width, height;
		scene_node_get_size(node, &width, &height);
		intersects = lx >= 0 && lx < width && ly >= 0 && ly < height;
		break;
	}

	if (intersects) {
		if (nx != NULL) {
			*nx = lx;
		}
		if (ny != NULL) {
			*ny = ly;
		}
		return node;
	}

	return NULL;
}

static void scissor_output(struct wlr_output *wlr_output, pixman_box32_t *rect)
{
	struct wlr_renderer *renderer = wlr_output->renderer;
	assert(renderer);

	struct wlr_box box = {
		.x = rect->x1,
		.y = rect->y1,
		.width = rect->x2 - rect->x1,
		.height = rect->y2 - rect->y1,
	};

	int ow, oh;
	wlr_output_transformed_resolution(wlr_output, &ow, &oh);

	enum wl_output_transform transform =
		wlr_output_transform_invert(wlr_output->transform);
	wlr_box_transform(&box, &box, transform, ow, oh);

	wlr_renderer_scissor(renderer, &box);
}

static void render_texture(struct wlr_output *wlr_output,
		pixman_region32_t *output_damage, struct wlr_texture *texture,
		const struct wlr_fbox *src_box, const struct wlr_box *dst_box,
		const float matrix[static 9])
{
	struct wlr_renderer *renderer = wlr_output->renderer;
	assert(renderer);

	struct wlr_fbox default_src_box = {0};
	if (wlr_fbox_empty(src_box)) {
		default_src_box.width = dst_box->width;
		default_src_box.height = dst_box->height;
		src_box = &default_src_box;
	}

	pixman_region32_t damage;
	pixman_region32_init(&damage);
	pixman_region32_init_rect(&damage, dst_box->x, dst_box->y,
		dst_box->width, dst_box->height);
	pixman_region32_intersect(&damage, &damage, output_damage);

	int nrects;
	pixman_box32_t *rects = pixman_region32_rectangles(&damage, &nrects);
	for (int i = 0; i < nrects; ++i) {
		scissor_output(wlr_output, &rects[i]);
		wlr_render_subtexture_with_matrix(renderer, texture, src_box, matrix, 1.0);
	}

	pixman_region32_fini(&damage);
}

static void render_border(struct wlr_output *wlr_output,
		pixman_region32_t *output_damage, struct utwc_scene_view *scene_view,
		const struct wlr_box *box, const float matrix[static 9])
{
	struct wlr_renderer *renderer = wlr_output->renderer;
	assert(renderer);

	pixman_region32_t damage;
	pixman_region32_init(&damage);
	pixman_region32_union_rect(&damage, &damage, box->x, box->y, box->width,
			scene_view->border.width[UTWC_BORDER_UP]);
	pixman_region32_union_rect(&damage, &damage, box->x, box->y + box->height -
			scene_view->border.width[UTWC_BORDER_DOWN], box->width,
			scene_view->border.width[UTWC_BORDER_DOWN]);
	pixman_region32_union_rect(&damage, &damage, box->x, box->y,
			scene_view->border.width[UTWC_BORDER_LEFT], box->height);
	pixman_region32_union_rect(&damage, &damage, box->x + box->width -
			scene_view->border.width[UTWC_BORDER_RIGHT], box->y,
			scene_view->border.width[UTWC_BORDER_RIGHT], box->height);
	pixman_region32_intersect(&damage, &damage, output_damage);

	int nrects;
	pixman_box32_t *rects = pixman_region32_rectangles(&damage, &nrects);
	for (int i = 0; i < nrects; ++i) {
		scissor_output(wlr_output, &rects[i]);
		wlr_render_rect(renderer, box, scene_view->border.color, matrix);
	}

	pixman_region32_fini(&damage);
}

struct render_data {
	pixman_region32_t *damage;
	pixman_region32_t *effective_damage;
	pixman_region32_t damage_store;
};

static void render_node_iterator(struct utwc_scene_node *node,
		int x, int y, void *_data)
{
	struct render_data *data = _data;
	struct wlr_output *wlr_output = G_output.wlr_output;

	if(node->type != UTWC_SCENE_NODE_VIEW &&
			node->state.inherit_crop == false &&
			data->effective_damage == &data->damage_store) {
		pixman_region32_fini(data->effective_damage);
		data->effective_damage = data->damage;
	}

	pixman_region32_t *effective_damage = data->effective_damage;

	struct wlr_box dst_box = {
		.x = x,
		.y = y,
	};
	scene_node_get_size(node, &dst_box.width, &dst_box.height);
	scale_box(&dst_box, wlr_output->scale);

	struct wlr_texture *texture;
	float matrix[9];
	enum wl_output_transform transform;

	switch (node->type) {
	case UTWC_SCENE_NODE_ROOT:
	case UTWC_SCENE_NODE_TREE:
		/* Root or tree node has nothing to render itself */
		break;
	case UTWC_SCENE_NODE_SURFACE:;
		struct utwc_scene_surface *scene_surface = utwc_scene_surface_from_node(node);
		struct wlr_surface *surface = scene_surface->surface;

		texture = wlr_surface_get_texture(surface);
		if (texture == NULL) {
			return;
		}

		transform = wlr_output_transform_invert(surface->current.transform);
		wlr_matrix_project_box(matrix, &dst_box, transform, 0.0,
			wlr_output->transform_matrix);

		struct wlr_fbox src_box = {0};
		wlr_surface_get_buffer_source_box(surface, &src_box);

		render_texture(wlr_output, effective_damage, texture,
			&src_box, &dst_box, matrix);

		if (wlr_output != NULL) {
			wlr_presentation_surface_sampled_on_output(G_server.wlr_presentation,
				surface, wlr_output);
		}
		break;
	case UTWC_SCENE_NODE_VIEW:;
		struct utwc_scene_view *scene_view = scene_view_from_node(node);
		render_border(wlr_output, effective_damage, scene_view,
				&dst_box, wlr_output->transform_matrix);

		struct wlr_box crop = scene_view->crop;

		if(crop.width == 0 && crop.height == 0)
			break;

		if(crop.width == 0) {
			crop.x = 0;
			crop.width = wlr_output->width;
		} else {
			crop.x += x;
		}

		if(crop.height == 0) {
			crop.y = 0;
			crop.height = wlr_output->height;
		} else {
			crop.y += x;
		}

		effective_damage = data->effective_damage = &data->damage_store;
		pixman_region32_init(effective_damage);
		pixman_region32_union_rect(effective_damage, effective_damage, crop.x,
				crop.y, crop.width, crop.height);
		pixman_region32_intersect(effective_damage, effective_damage,
				data->damage);
		break;
	}
}

static void scene_node_for_each_node(struct utwc_scene_node *node,
		int lx, int ly, utwc_scene_node_iterator_func_t user_iterator,
		void *user_data)
{
	if (!node->state.enabled) {
		return;
	}

	lx += node->state.x;
	ly += node->state.y;

	user_iterator(node, lx, ly, user_data);

	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		scene_node_for_each_node(child, lx, ly, user_iterator, user_data);
	}
}

void utwc_scene_render_output(pixman_region32_t *damage)
{
	pixman_region32_t full_region;
	pixman_region32_init_rect(&full_region, 0, 0, G_output.wlr_output->width,
			G_output.wlr_output->height);
	if (damage == NULL) {
		damage = &full_region;
	}

	if (G_output.wlr_output->enabled && pixman_region32_not_empty(damage)) {
		struct render_data data = {
			.damage = damage,
			.effective_damage = damage,
			.damage_store = {},
		};
		scene_node_for_each_node(&G_scene.node, 0, 0, render_node_iterator,
			&data);
		wlr_renderer_scissor(G_server.wlr_renderer, NULL);

		if(data.effective_damage == &data.damage_store)
			pixman_region32_fini(data.effective_damage);
	}

	pixman_region32_fini(&full_region);
}

static void scene_output_send_enter_iterator(struct wlr_surface *surface,
		int sx, int sy, void *data)
{
	(void)sx;
	(void)sy;
	struct wlr_output *wlr_output = data;
	wlr_surface_send_enter(surface, wlr_output);
}

// XXX: move some of this over somewhere else
bool utwc_scene_output_create(struct wlr_output *wlr_output)
{
	G_output.damage = wlr_output_damage_create(wlr_output);
	if (G_output.damage == NULL)
		return false;

	G_output.wlr_output = wlr_output;

	wlr_output_damage_add_whole(G_output.damage);

	utwc_scene_output_for_each_surface(scene_output_send_enter_iterator,
		G_output.wlr_output);

	return true;
}

static void scene_output_send_leave_iterator(struct wlr_surface *surface,
		int sx, int sy, void *data)
{
	(void)sx;
	(void)sy;
	struct wlr_output *wlr_output = data;
	wlr_surface_send_leave(surface, wlr_output);
}

void utwc_scene_output_destroy(void)
{
	if(G_output.wlr_output) {
		utwc_scene_output_for_each_surface(scene_output_send_leave_iterator,
			G_output.wlr_output);
		G_output.wlr_output = NULL;
	}
}

struct check_scanout_data {
	// in
	struct wlr_box viewport_box;
	// out
	struct utwc_scene_node *node;
	size_t n;
};

static void check_scanout_iterator(struct utwc_scene_node *node,
		int x, int y, void *_data)
{
	struct check_scanout_data *data = _data;

	struct wlr_box node_box = { .x = x, .y = y };
	scene_node_get_size(node, &node_box.width, &node_box.height);

	struct wlr_box intersection;
	if (!wlr_box_intersection(&intersection, &data->viewport_box, &node_box)) {
		return;
	}

	data->n++;

	if (data->viewport_box.x == node_box.x &&
			data->viewport_box.y == node_box.y &&
			data->viewport_box.width == node_box.width &&
			data->viewport_box.height == node_box.height) {
		data->node = node;
	}
}

static bool scene_output_scanout(void)
{
	struct wlr_box viewport_box = { .x = 0, .y = 0 };
	wlr_output_effective_resolution(G_output.wlr_output,
		&viewport_box.width, &viewport_box.height);

	struct check_scanout_data check_scanout_data = {
		.viewport_box = viewport_box,
	};
	scene_node_for_each_node(&G_scene.node, 0, 0,
		check_scanout_iterator, &check_scanout_data);
	if (check_scanout_data.n != 1 || check_scanout_data.node == NULL) {
		return false;
	}

	struct utwc_scene_node *node = check_scanout_data.node;
	struct wlr_buffer *buffer;
	switch (node->type) {
	case UTWC_SCENE_NODE_SURFACE:;
		struct utwc_scene_surface *scene_surface = utwc_scene_surface_from_node(node);
		if (scene_surface->surface->buffer == NULL ||
				scene_surface->surface->current.viewport.has_src ||
				scene_surface->surface->current.transform !=
				G_output.wlr_output->transform) {
			return false;
		}
		buffer = &scene_surface->surface->buffer->base;
		break;
	default:
		return false;
	}

	wlr_output_attach_buffer(G_output.wlr_output, buffer);
	if (!wlr_output_test(G_output.wlr_output)) {
		wlr_output_rollback(G_output.wlr_output);
		return false;
	}

	if (node->type == UTWC_SCENE_NODE_SURFACE) {
		struct utwc_scene_surface *scene_surface =
			utwc_scene_surface_from_node(node);
		// Since outputs may overlap, we still need to check this even though
		// we know that the surface size matches the size of this G_output.
		if (G_output.wlr_output != NULL) {
			wlr_presentation_surface_sampled_on_output(G_server.wlr_presentation,
				scene_surface->surface, G_output.wlr_output);
		}
	}

	return wlr_output_commit(G_output.wlr_output);
}

bool utwc_scene_output_commit(void)
{
	struct wlr_renderer *renderer = G_output.wlr_output->renderer;
	assert(renderer != NULL);

	bool scanout = scene_output_scanout();
	if (scanout != G_output.prev_scanout) {
		UTWC_LOG(UTWC_LOG_DEBUG, "Direct scan-out %s",
			scanout ? "enabled" : "disabled");
		// When exiting direct scan-out, damage everything
		wlr_output_damage_add_whole(G_output.damage);
	}
	G_output.prev_scanout = scanout;
	if (scanout) {
		return true;
	}

	bool needs_frame;
	pixman_region32_t damage;
	pixman_region32_init(&damage);
	if (!wlr_output_damage_attach_render(G_output.damage,
			&needs_frame, &damage)) {
		pixman_region32_fini(&damage);
		return false;
	}

	if (!needs_frame) {
		pixman_region32_fini(&damage);
		wlr_output_rollback(G_output.wlr_output);
		return true;
	}

	wlr_renderer_begin(renderer, G_output.wlr_output->width,
		G_output.wlr_output->height);

	int nrects;
	pixman_box32_t *rects = pixman_region32_rectangles(&damage, &nrects);
	for (int i = 0; i < nrects; ++i) {
		scissor_output(G_output.wlr_output, &rects[i]);
		wlr_renderer_clear(renderer, (float[4]){ 0.2, 0.2, 0.2, 1.0 });
	}

	utwc_scene_render_output(&damage);
	wlr_output_render_software_cursors(G_output.wlr_output, &damage);

	wlr_renderer_end(renderer);
	pixman_region32_fini(&damage);

	int tr_width, tr_height;
	wlr_output_transformed_resolution(G_output.wlr_output, &tr_width,
		&tr_height);

	enum wl_output_transform transform =
		wlr_output_transform_invert(G_output.wlr_output->transform);

	pixman_region32_t frame_damage;
	pixman_region32_init(&frame_damage);
	wlr_region_transform(&frame_damage, &G_output.damage->current,
		transform, tr_width, tr_height);
	wlr_output_set_damage(G_output.wlr_output, &frame_damage);
	pixman_region32_fini(&frame_damage);

	return wlr_output_commit(G_output.wlr_output);
}

static void scene_output_send_frame_done_iterator(struct utwc_scene_node *node,
		struct wlr_output *wlr_output, struct timespec *now)
{
	if (!node->state.enabled) {
		return;
	}

	if (node->type == UTWC_SCENE_NODE_SURFACE) {
		struct utwc_scene_surface *scene_surface =
			utwc_scene_surface_from_node(node);
		if (wlr_output != NULL) {
			wlr_surface_send_frame_done(scene_surface->surface, now);
		}
	}

	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		scene_output_send_frame_done_iterator(child, wlr_output, now);
	}
}

void utwc_scene_output_send_frame_done(struct timespec *now)
{
	scene_output_send_frame_done_iterator(&G_scene.node, G_output.wlr_output, now);
}

static void scene_output_for_each_surface(const struct wlr_box *output_box,
		struct utwc_scene_node *node, int lx, int ly,
		wlr_surface_iterator_func_t user_iterator, void *user_data)
{
	if (!node->state.enabled) {
		return;
	}

	lx += node->state.x;
	ly += node->state.y;

	if (node->type == UTWC_SCENE_NODE_SURFACE) {
		struct wlr_box node_box = { .x = lx, .y = ly };
		scene_node_get_size(node, &node_box.width, &node_box.height);

		struct wlr_box intersection;
		if (wlr_box_intersection(&intersection, output_box, &node_box)) {
			struct utwc_scene_surface *scene_surface =
				utwc_scene_surface_from_node(node);
			user_iterator(scene_surface->surface, lx, ly, user_data);
		}
	}

	struct utwc_scene_node *child;
	wl_list_for_each(child, &node->state.children, state.link) {
		scene_output_for_each_surface(output_box, child, lx, ly,
			user_iterator, user_data);
	}
}

void utwc_scene_output_for_each_surface( wlr_surface_iterator_func_t iterator,
		void *user_data)
{
	struct wlr_box box = { .x = 0, .y = 0 };
	wlr_output_effective_resolution(G_output.wlr_output,
		&box.width, &box.height);
	scene_output_for_each_surface(&box, &G_scene.node, 0, 0,
		iterator, user_data);
}

static void scene_xdg_toplevel_resize(struct utwc_scene_view *scene_view)
{
	struct wlr_box geo;
	wlr_xdg_surface_get_geometry(scene_view->xdg_surface, &geo);

	int width = geo.width;
	int height = geo.height;

	if(scene_view->forced_width != 0) {
		scene_view->real_width = scene_view->forced_width;
		width = scene_view->forced_width;
		width -= scene_view->border.width[UTWC_BORDER_LEFT];
		width -= scene_view->border.width[UTWC_BORDER_RIGHT];
	} else {
		scene_view->real_width = geo.width;
		scene_view->real_width += scene_view->border.width[UTWC_BORDER_LEFT];
		scene_view->real_width += scene_view->border.width[UTWC_BORDER_RIGHT];
	}
	if(scene_view->forced_height != 0) {
		scene_view->real_height = scene_view->forced_height;
		height = scene_view->forced_height;
		height -= scene_view->border.width[UTWC_BORDER_UP];
		height -= scene_view->border.width[UTWC_BORDER_DOWN];
	} else {
		scene_view->real_height = geo.height;
		scene_view->real_height += scene_view->border.width[UTWC_BORDER_UP];
		scene_view->real_height += scene_view->border.width[UTWC_BORDER_DOWN];
	}

	if(width != geo.width || height != geo.height) {
		wlr_xdg_toplevel_set_size(scene_view->xdg_surface, width,
				height);
	}

	int x = 0;
	int y = 0;

	x += scene_view->border.width[UTWC_BORDER_LEFT];
	y += scene_view->border.width[UTWC_BORDER_UP];
	x += (width - geo.width) / 2;
	y += (height - geo.height) / 2;

	scene_view->crop = (struct wlr_box){
		.x = 0,
		.y = 0,
		.width = 0,
		.height = 0,
	};

	if(geo.width > width) {
		scene_view->crop.x = scene_view->border.width[UTWC_BORDER_LEFT];
		scene_view->crop.width = width;
	}

	if(geo.height > height) {
		scene_view->crop.y = scene_view->border.width[UTWC_BORDER_UP];
		scene_view->crop.height = height;
	}

	utwc_scene_node_set_position(scene_view->tree, x, y);
}

static void scene_view_handle_xdg_surface_map(struct wl_listener *listener,
		void *data)
{
	(void)data;
	struct utwc_scene_view *scene_view =
		wl_container_of(listener, scene_view, xdg_surface_map);
	utwc_scene_node_set_enabled(&scene_view->node, true);
	scene_xdg_toplevel_resize(scene_view);
}

static void scene_view_handle_xdg_surface_unmap(struct wl_listener *listener,
		void *data)
{
	(void)data;
	struct utwc_scene_view *scene_view =
		wl_container_of(listener, scene_view, xdg_surface_unmap);
	utwc_scene_node_set_enabled(&scene_view->node, false);
}

static void scene_view_handle_xdg_surface_commit(struct wl_listener *listener,
		void *data)
{
	(void)data;
	struct utwc_scene_view *scene_view =
		wl_container_of(listener, scene_view, xdg_surface_commit);
	scene_xdg_toplevel_resize(scene_view);
}

static void scene_view_handle_xdg_surface_destroy(struct wl_listener *listener,
		void *data)
{
	(void)data;
	struct utwc_scene_view *scene_view =
		wl_container_of(listener, scene_view, xdg_surface_destroy);
	utwc_scene_node_destroy(&scene_view->node);
}

struct utwc_scene_view *utwc_scene_view_create(struct utwc_scene_node *parent,
		struct wlr_xdg_surface *xdg_surface)
{
	assert(xdg_surface->role == WLR_XDG_SURFACE_ROLE_TOPLEVEL);

	struct utwc_scene_view *scene_view =
		calloc(1, sizeof(*scene_view));
	if (scene_view == NULL) {
		return NULL;
	}

	scene_node_init(&scene_view->node, UTWC_SCENE_NODE_VIEW, parent);
	scene_view->tree =
		utwc_scene_xdg_surface_create(&scene_view->node, xdg_surface);
	scene_view->xdg_surface = xdg_surface;

	utwc_scene_node_set_inherit_crop(scene_view->tree, true);

	scene_view->border = (struct utwc_scene_view_border){
		.width = {0, 0, 0, 0},
		.color = {1.0f, 0.0f, 0.0f, 1.0f},
	};

	scene_view->forced_width = 0;
	scene_view->forced_height = 0;

	scene_view->real_width = 0;
	scene_view->real_height = 0;

	scene_view->crop = (struct wlr_box){
		.x = 0,
		.y = 0,
		.width = 0,
		.height = 0,
	};

	scene_view->xdg_surface_map.notify =
		scene_view_handle_xdg_surface_map;
	wl_signal_add(&xdg_surface->events.map, &scene_view->xdg_surface_map);

	scene_view->xdg_surface_unmap.notify =
		scene_view_handle_xdg_surface_unmap;
	wl_signal_add(&xdg_surface->events.unmap, &scene_view->xdg_surface_unmap);

	scene_view->xdg_surface_commit.notify =
		scene_view_handle_xdg_surface_commit;
	wl_signal_add(&xdg_surface->surface->events.commit,
		&scene_view->xdg_surface_commit);

	scene_view->xdg_surface_destroy.notify =
		scene_view_handle_xdg_surface_destroy;
	wl_signal_add(&xdg_surface->surface->events.destroy,
			&scene_view->xdg_surface_destroy);

	utwc_scene_node_set_enabled(&scene_view->node, xdg_surface->mapped);

	if(xdg_surface->mapped) {
		scene_xdg_toplevel_resize(scene_view);
	}

	return scene_view;
}

void utwc_scene_view_set_size(struct utwc_scene_view *scene_view, int width,
		int height)
{
	scene_view->forced_width = width;
	scene_view->forced_height = height;
	scene_xdg_toplevel_resize(scene_view);
}

void utwc_scene_view_set_border_width(struct utwc_scene_view *scene_view,
		unsigned int width[4])
{
	memcpy(scene_view->border.width, width, sizeof(int [4]));
	scene_xdg_toplevel_resize(scene_view);
	scene_node_damage_whole(&scene_view->node);
}

void utwc_scene_view_set_border_color(struct utwc_scene_view *scene_view,
		float color[4])
{
	memcpy(scene_view->border.color, color, sizeof(float [4]));
	scene_node_damage_whole(&scene_view->node);
}
