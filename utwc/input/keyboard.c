#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/backend/multi.h>
#include <wlr/backend/session.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_seat.h>
#include <xkbcommon/xkbcommon.h>
#include "desktop.h"
#include "input/keybind.h"
#include "input/seat.h"
#include "log.h"
#include "server.h"
#include "util.h"

#include "input/keyboard.h"

static void handle_keyboard_modifiers(struct wl_listener *listener, void *data)
{
	(void)data;
	struct utwc_keyboard *const keyboard = wl_container_of(listener, keyboard,
		e_modifiers);
	struct wlr_seat *const wlr_seat = G_seat.wlr_seat;

	//TODO: handle keybindings

	wlr_seat_set_keyboard(wlr_seat, keyboard->device);
	wlr_seat_keyboard_notify_modifiers(wlr_seat,
		&keyboard->device->keyboard->modifiers);
}

bool handle_builtin_bindings(struct utwc_keyboard *keyboard,
		const xkb_keysym_t *keysyms, size_t nsyms, uint32_t modifiers)
{
	(void)modifiers;
	(void)keyboard;

	size_t i;

	for(i = 0; i < nsyms; i++) {
		if(keysyms[i] >= XKB_KEY_XF86Switch_VT_1 &&
				keysyms[i] <=  XKB_KEY_XF86Switch_VT_12) {
			if(wlr_backend_is_multi(G_server.wlr_backend)) {
				struct wlr_session *session =
					wlr_backend_get_session(G_server.wlr_backend);
				if(session) {
					unsigned vt = keysyms[i] -
						XKB_KEY_XF86Switch_VT_1 + 1;
					wlr_session_change_vt(session, vt);
				}
			}

			return true;
		} else if((modifiers & WLR_MODIFIER_ALT) && keysyms[i] == XKB_KEY_n) {
			utwc_desktop_focus_next();
			return true;
		} else if((modifiers & WLR_MODIFIER_ALT) && keysyms[i] == XKB_KEY_p) {
			utwc_desktop_focus_prev();
			return true;
		} else if((modifiers & WLR_MODIFIER_ALT) && keysyms[i] == XKB_KEY_h) {
			struct utwc_view_rel_box box = {
				.x = { 0, 1 },
				.y = { 0, 1 },
				.width = { 1, 2},
				.height = { 1, 1 },
			};
			if(G_desktop.activated)
				utwc_view_set_current_box(G_desktop.activated, &box);
			return true;
		} else if(keysyms[i] >= XKB_KEY_0 && keysyms[i] <= XKB_KEY_9) {
			char mark[] = { (char)(keysyms[i] - XKB_KEY_0) + '0', '\0' };
			if(modifiers & WLR_MODIFIER_CTRL) {
				if(modifiers & WLR_MODIFIER_ALT) {
						utwc_view_remove_mark(G_desktop.activated, mark);
				} else {
					if(G_desktop.activated)
						utwc_view_add_mark(G_desktop.activated, mark, false);
				}
				return true;
			} else if(modifiers & WLR_MODIFIER_ALT) {
				utwc_desktop_goto_mark(mark);
				return true;
			}
		} else if((modifiers & WLR_MODIFIER_ALT) && keysyms[i] == XKB_KEY_q) {
			wl_display_terminate(G_server.wl_display);
		}
	}
	return false;
}

static void handle_keyboard_key(struct wl_listener *listener, void *data)
{
	struct utwc_keyboard *const keyboard = wl_container_of(listener, keyboard,
		e_key);
	struct wlr_event_keyboard_key *const event = data;
	struct wlr_seat *const wlr_seat = G_seat.wlr_seat;
	struct wlr_input_device *device = keyboard->device;

	const xkb_keysym_t *keysyms;
	xkb_keycode_t keycode = event->keycode + 8;

	int nsyms = xkb_state_key_get_syms(device->keyboard->xkb_state,
		keycode, &keysyms);

	uint32_t modifiers = wlr_keyboard_get_modifiers(device->keyboard);

	if(event->state == WL_KEYBOARD_KEY_STATE_PRESSED) {
		if(handle_builtin_bindings(keyboard, keysyms, nsyms, modifiers))
			return;
		if(utwc_keybind_do_key(keysyms, nsyms, modifiers))
			return;
	}

	//TODO: handle keybindings

	wlr_seat_set_keyboard(wlr_seat, keyboard->device);
	wlr_seat_keyboard_notify_key(wlr_seat, event->time_msec,
		event->keycode, event->state);
}

void utwc_keyboard_fini(struct utwc_keyboard *keyboard)
{
	wl_list_remove(&keyboard->link);
	wl_list_remove(&keyboard->e_modifiers.link);
	wl_list_remove(&keyboard->e_key.link);
	wl_list_remove(&keyboard->e_destroy.link);
	free(keyboard);
}

static void handle_keyboard_destroy(struct wl_listener *listener, void *data)
{
	(void)data;
	struct utwc_keyboard *const keyboard = wl_container_of(listener, keyboard,
		e_destroy);

	utwc_keyboard_fini(keyboard);
}

void utwc_keyboard_handle_new(struct wlr_input_device *device)
{
	struct utwc_keyboard *keyboard = utwc_malloc(sizeof(struct utwc_keyboard));

	struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if(!context) {
		UTWC_LOG(UTWC_LOG_ERROR, "Unable to create XKB context");
		return;
	}
	struct xkb_keymap *keymap = xkb_keymap_new_from_names(context, NULL,
			XKB_KEYMAP_COMPILE_NO_FLAGS);
	if(!context) {
		UTWC_LOG(UTWC_LOG_ERROR, "Unable to create XKB keymap");
		return;
	}

	wlr_keyboard_set_keymap(device->keyboard, keymap);
	xkb_keymap_unref(keymap);
	xkb_context_unref(context);
	wlr_keyboard_set_repeat_info(device->keyboard, 25, 600);

	HANDLE_SIGNAL(keyboard->e_modifiers,
			handle_keyboard_modifiers,
			device->keyboard->events.modifiers);
	HANDLE_SIGNAL(keyboard->e_key,
			handle_keyboard_key,
			device->keyboard->events.key);
	HANDLE_SIGNAL(keyboard->e_destroy,
			handle_keyboard_destroy,
			device->keyboard->events.destroy);

	wl_list_insert(&G_seat.keyboards, &keyboard->link);
	keyboard->device = device;
	if(!wlr_seat_get_keyboard(G_seat.wlr_seat))
		wlr_seat_set_keyboard(G_seat.wlr_seat, device);
}
