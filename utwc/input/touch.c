#define _POSIX_C_SOURCE 201112L

#include <linux/input-event-codes.h>
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_touch.h>
#include <wlr/util/box.h>
#include "desktop.h"
#include "input/gesture.h"
#include "log.h"
#include "output.h"
#include "scene.h"
#include "input/seat.h"
#include "util.h"

#include "input/touch.h"

struct utwc_G_touch G_touch;

static void handle_touch_destroy(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	//TODO: handle
}

static void handle_touch_down(struct wl_listener *listener, void *data)
{
	(void)listener;
	struct wlr_event_touch_down *const event = data;
	struct wlr_seat *const wlr_seat = G_seat.wlr_seat;

	int sx, sy;
	struct wlr_surface *surface = utwc_gesture_do_touch_down(event, &sx, &sy);

	if(surface) {
		if(wlr_surface_accepts_touch(wlr_seat, surface)) {
			// Give touch input to the surface.
			wlr_seat_touch_notify_down(wlr_seat, surface,
				event->time_msec, event->touch_id, sx, sy);
		} else if(!G_touch.has_pointer_id) {
			//TODO: Change pointer simulation behavior and make it
			//configurable.

			// Simulate a pointer left-click.
			wlr_seat_pointer_notify_enter(wlr_seat, surface, sx, sy);
			wlr_seat_pointer_notify_motion(wlr_seat, event->time_msec, sx, sy);
			wlr_seat_pointer_notify_button(wlr_seat,
				event->time_msec, BTN_LEFT, WLR_BUTTON_PRESSED);

			// Remember this touch id and use it as a pointer later.
			G_touch.has_pointer_id = true;
			G_touch.pointer_id = event->touch_id;
		}
	}
}

static void handle_touch_motion(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	//TODO: handle
	struct wlr_event_touch_motion *const event = data;
	struct wlr_seat *const wlr_seat = G_seat.wlr_seat;

	int sx, sy;
	struct wlr_surface *surface = utwc_gesture_do_touch_motion(event, &sx, &sy);

	if(surface) {
		if(wlr_surface_accepts_touch(wlr_seat, surface)) {
			// Give touch input to the surface.
			wlr_seat_touch_point_focus(wlr_seat, surface,
				event->time_msec, event->touch_id, sx, sy);
			wlr_seat_touch_notify_motion(wlr_seat,
				event->time_msec, event->touch_id, sx, sy);
		} else if(G_touch.has_pointer_id &&
				G_touch.pointer_id == event->touch_id) {
			// Simulate a pointer move event.
			wlr_seat_pointer_notify_enter(wlr_seat, surface, sx, sy);
			wlr_seat_pointer_notify_motion(wlr_seat, event->time_msec, sx, sy);
		}
	}
}

static void handle_touch_up(struct wl_listener *listener, void *data)
{
	(void)listener;
	struct wlr_event_touch_up *const event = data;
	struct wlr_seat *const wlr_seat = G_seat.wlr_seat;

	if(!utwc_gesture_do_touch_up(event))
		return;

	wlr_seat_touch_notify_up(wlr_seat, event->time_msec, event->touch_id);

	if(G_touch.has_pointer_id && G_touch.pointer_id == event->touch_id) {
		// Simulate a pointer left-click release.
		wlr_seat_pointer_notify_button(wlr_seat,event->time_msec,
			BTN_LEFT, WLR_BUTTON_RELEASED);
		wlr_seat_pointer_notify_clear_focus(wlr_seat);

		G_touch.has_pointer_id = false;
		G_touch.pointer_id = 0;
	}
}

void utwc_touch_handle_new(struct wlr_input_device *device)
{
	//A usable output doesn't exist, wait until one does to configure.
	if(G_output.wlr_output == NULL) {
		struct utwc_pending_touch *pending_touch =
			utwc_malloc(sizeof(struct utwc_pending_touch));
		pending_touch->device = device;
		wl_list_insert(&G_touch.pending_devices, &pending_touch->link);
		return;
	}

	if(G_touch.device != NULL) {
		UTWC_LOG(UTWC_LOG_INFO, "Ignoring extra touch device %s",
			device->name);
	}

	G_touch.device = device;
	HANDLE_SIGNAL(G_touch.e_down,
			handle_touch_down,
			device->touch->events.down)
	HANDLE_SIGNAL(G_touch.e_motion,
			handle_touch_motion,
			device->touch->events.motion)
	HANDLE_SIGNAL(G_touch.e_up,
			handle_touch_up,
			device->touch->events.up)
	HANDLE_SIGNAL(G_touch.e_destroy,
			handle_touch_destroy,
			device->events.destroy)
}

void utwc_touch_handle_pending(void)
{
	if(G_output.wlr_output == NULL)
		return;

	struct utwc_pending_touch *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &G_touch.pending_devices, link) {
		utwc_process_input(pos->device);
		free(pos);
	}

	wl_list_init(&G_touch.pending_devices);
}
