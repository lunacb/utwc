#define _POSIX_C_SOURCE 201112L

#include <linux/input-event-codes.h>
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_touch.h>
#include <wlr/util/box.h>
#include "desktop.h"
#include "input/binding.h"
#include "input/touch.h"
#include "log.h"
#include "output.h"
#include "scene.h"
#include "util.h"

#include "input/gesture.h"

#ifndef M_PI
# define M_PI 3.14159265358979323846
#endif
#define DEG2RAD(x) ((double)(x)*M_PI/180)


/**
 * Tracks a single touch point as a canidate for a gesture.
 */
struct finger {
	int32_t id;
	int start_x;
	int start_y;
	int x;
	int y;
};

struct {
	struct finger fingers[20];
	// Position in `fingers`.
	unsigned int fpos;
	// Total points currently on the screen.
	unsigned int n_points;
	struct timespec begin;

	bool invalidated;
	// True if a finger has been released and `n_points` > 0.
	bool going_down;
} touch_ctx = {
	.fpos = 0,
	.n_points = 0,
	.invalidated = false,
	.going_down = false,
};

static enum utwc_gesture_region get_region_at(int x, int y)
{
	enum utwc_gesture_region res = 0;

	int cx = x - G_desktop.viewspace.x;
	int cy = y - G_desktop.viewspace.y;
	res |= (cx < G_desktop.viewspace.width/2)?GESTURE_DIR_W:GESTURE_DIR_E;
	res |= (cy < G_desktop.viewspace.height/2)?GESTURE_DIR_N:GESTURE_DIR_S;

	bool w = (x <= G_desktop.viewspace.x);
	bool n = (y <= G_desktop.viewspace.y);
	bool e = (x >= G_desktop.viewspace.x + G_desktop.viewspace.width);
	bool s = (y >= G_desktop.viewspace.y + G_desktop.viewspace.height);

	bool center = false;

	if(n) {
		if     (e) res |= GESTURE_EDGE_N | GESTURE_EDGE_E;
		else if(w) res |= GESTURE_EDGE_N | GESTURE_EDGE_W;
		else       res |= GESTURE_EDGE_N | GESTURE_MIDDLE_EDGE;
	} else if(s) {
		if     (e) res |= GESTURE_EDGE_S | GESTURE_EDGE_E;
		else if(w) res |= GESTURE_EDGE_S | GESTURE_EDGE_W;
		else       res |= GESTURE_EDGE_S | GESTURE_MIDDLE_EDGE;
	} else {
		res |= GESTURE_MIDDLE_EDGE;

		if     (e) res |= GESTURE_EDGE_E;
		else if(w) res |= GESTURE_EDGE_W;
		else       center = true;
	}

	res |= center?GESTURE_CENTER:GESTURE_EDGE;

	return res;
}

// TODO: optimize
static enum utwc_gesture_region get_relative_region_for(int start_x,
		int start_y, int x, int y)
{
	double rad = atan2(x - start_x, y - start_y);

	if(rad <= DEG2RAD(-157.5))
		return GESTURE_REL_N;
	else if(rad <= DEG2RAD(-112.5))
		return GESTURE_REL_NW;
	else if(rad <= DEG2RAD(-67.5))
		return GESTURE_REL_W;
	else if(rad <= DEG2RAD(-22.5))
		return GESTURE_REL_SW;
	else if(rad <= DEG2RAD(22.5))
		return GESTURE_REL_S;
	else if(rad <= DEG2RAD(67.5))
		return GESTURE_REL_SE;
	else if(rad <= DEG2RAD(112.5))
		return GESTURE_REL_E;
	else if(rad <= DEG2RAD(157.5))
		return GESTURE_REL_NE;
	else
		return GESTURE_REL_N;
}

/**
 * Convert touch point coordinates to scene-local ones.
 */
static void absolute_to_scene_local(double x, double y, int *sx, int *sy)
{
	struct wlr_box point_box = {
		.x = G_output.wlr_output->width * x / G_output.wlr_output->scale,
		.y = G_output.wlr_output->height * y / G_output.wlr_output->scale,
		.width = 0,
		.height = 0,
	};

	// this function operates on a box instead of a point like we need, but we
	// can just make a box with 0 area
	wlr_box_transform(&point_box, &point_box,
			G_output.wlr_output->transform,
			G_output.wlr_output->width / G_output.wlr_output->scale,
			G_output.wlr_output->height / G_output.wlr_output->scale);

	*sx = point_box.x;
	*sy = point_box.y;
}

/**
 * Returns the `surface` at `x` and `y`, or NULL. `rnode` is the node that
 * surface is contained in and `sx` and `sy` are the coordinates converted to
 * surface-local space.
 */
static void surface_at_local_coords(int x, int y, struct wlr_surface **surface,
		struct utwc_scene_node **rnode, int *sx, int *sy)
{
	*sx = 0;
	*sy = 0;
	*surface = NULL;
	*rnode = NULL;

	double nx, ny;
	struct utwc_scene_node *node =
		utwc_scene_node_at(&G_scene.node, x, y, &nx, &ny);

	if(node == NULL || node->type != UTWC_SCENE_NODE_SURFACE)
		return;

	struct utwc_scene_surface *scene_surface =
		utwc_scene_surface_from_node(node);

	*sx = (int)nx;
	*sy = (int)ny;
	*surface = scene_surface->surface;
	*rnode = node;
}

/**
 * Test if the difference between `begin` and `end` is between `min` and `max`,
 * inclusive.
 * 	`min` and `max` can have their `tv_sec` fileds set to a negative number to
 * 	be ignored.
 */
static bool time_is_between(struct timespec *begin, struct timespec *end,
		struct timespec *min, struct timespec *max)
{
	struct timespec time = *end;
	time.tv_sec -= begin->tv_sec;
	if(begin->tv_nsec > time.tv_nsec) {
		time.tv_sec--;
		time.tv_nsec = 1000000000 - (begin->tv_nsec - time.tv_nsec);
	} else {
		time.tv_nsec -= begin->tv_nsec;
	}

	if(min->tv_sec >= 0) {
		if(time.tv_sec == min->tv_sec) {
			if(time.tv_nsec < min->tv_nsec)
				return false;
		} else if(time.tv_sec < min->tv_sec) {
			return false;
		}
	}

	if(max->tv_sec >= 0) {
		if(time.tv_sec == max->tv_sec) {
			if(time.tv_nsec > max->tv_nsec)
				return false;
		} else if(time.tv_sec > max->tv_sec) {
			return false;
		}
	}

	return true;
}

static struct utwc_rule_watcher *create_watcher(struct utwc_rule *rule)
{
	struct utwc_rule_watcher watcher = {
		.rule = *rule,
		.executor = { .type = UTWC_RULE_EXECUTOR_NONE },
	};

	return utwc_watcher_add(&watcher);
}

bool utwc_gesture_add_binding(struct utwc_gesture_binding *binding,
		struct utwc_binding_mode *mode, struct utwc_rule *start_rule,
		struct utwc_rule *end_rule)
{
	struct utwc_gesture_binding *new = utwc_malloc(sizeof(*new));
	*new = *binding;

	// If there are no touch points on the screen then this will be reset when
	// there are. Otherwise, this will stop the binding from being triggered
	// weirdly.
	new->invalidated = true;

	strcpy(new->human, "TODO");
	wl_list_insert(&mode->gesture, &new->link);
	new->surface_start_rule = (start_rule)?create_watcher(start_rule):NULL;
	new->surface_end_rule = (end_rule)?create_watcher(end_rule):NULL;

	return false;
}

void utwc_gesture_fini(struct utwc_gesture_binding *binding)
{
	utwc_command_chain_fini(&binding->command);

	wl_list_remove(&binding->link);
	free(binding);
}

static void handle_binding(struct utwc_gesture_binding *binding)
{
	UTWC_LOG(UTWC_LOG_DEBUG, "Handling gesture binding \"%s\"",
			binding->human);
	utwc_command_chain_execute(&binding->command);
}

static bool surface_matches_watcher_rule(struct utwc_scene_node *node,
		struct utwc_rule_watcher *watcher)
{
	if(watcher == NULL) // No rule.
		return true;

	// Look for the parent view node.
	for(; node != NULL; node = node->parent)
		if(node->type == UTWC_SCENE_NODE_VIEW)
			break;

	// true if node was initially NULL or somehow wasn't a child of a view, the
	// latter being a bug.
	if(node == NULL)
		return false;

	struct utwc_view *view = node->data;

	struct utwc_rule_index *pos;
	wl_list_for_each(pos, &watcher->matched, link)
		if(pos->target == &view->rule_target)
			return true;

	return false;
}

struct wlr_surface *utwc_gesture_do_touch_down(
		struct wlr_event_touch_down *event, int *ret_x, int *ret_y)
{
	int x, y, sx = 0, sy = 0;
	struct wlr_surface *surface = NULL;
	struct utwc_scene_node *surface_node = NULL;
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	touch_ctx.n_points++;

	// Gestures need an output for coordinates.
	if(G_output.wlr_output == NULL) {
		touch_ctx.invalidated = true;
		return NULL;
	}

	absolute_to_scene_local(event->x, event->y, &x, &y);
	surface_at_local_coords(x, y, &surface, &surface_node, &sx, &sy);
	*ret_x = sx;
	*ret_y = sy;


	// If this is the first finger down, initiate binding statuses.
	if(touch_ctx.n_points == 1) {
		struct utwc_gesture_binding *pos;
		wl_list_for_each(pos, &G_binding.current->gesture, link)
			pos->invalidated = false;
		touch_ctx.invalidated = false;
		touch_ctx.going_down = false;
		clock_gettime(CLOCK_MONOTONIC, &touch_ctx.begin);
	}

	// If invalidated then input for the new finger should go to a surface.
	if(touch_ctx.invalidated)
		return surface;

	// Would happen if a person touched the screen, took their finger off, and
	// put it on again whith another finger already on the entire time.
	if(touch_ctx.going_down) {
		UTWC_LOG(UTWC_LOG_DEBUG, "Touch points do not look like a gesture");
		touch_ctx.invalidated = true;
		return surface;
	}

	bool valid_found = false;
	struct utwc_gesture_binding *pos;
	wl_list_for_each(pos, &G_binding.current->gesture, link) {
		if(pos->invalidated)
			continue;

		if(!surface_matches_watcher_rule(surface_node,
					pos->surface_start_rule))
			pos->invalidated = true;

		// Check if there are more fingers down than specified by the binding.
		if(touch_ctx.fpos + 1 > pos->fingers)
			pos->invalidated = true;

		enum utwc_gesture_region region = get_region_at(x, y);
		if((region & pos->start) != pos->start)
			pos->invalidated = true;

		enum utwc_gesture_surface surf = pos->surface_start;
		if(surf != GESTURE_ON_ANY &&
				(surface != NULL) != (surf == GESTURE_ON_SURFACE))
			pos->invalidated = true;

		// Check if the existing fingers have been on the screen too long.
		struct timespec no_min = { -1, -1 };
		if(!time_is_between(&touch_ctx.begin, &now, &no_min,
					&pos->max_time))
			pos->invalidated = true;

		if(pos->invalidated) {
			UTWC_LOG(UTWC_LOG_DEBUG,
					"Gesture does not look like binding \"%s\"",
					pos->human);
		} else {
			valid_found = true;
			UTWC_LOG(UTWC_LOG_DEBUG, "Gesture binding \"%s\" got a finger",
					pos->human);
		}
	}

	if(touch_ctx.fpos == ARRLEN(touch_ctx.fingers)) {
		UTWC_LOG(UTWC_LOG_ERROR, "You have too many fingers!");
		touch_ctx.invalidated = true;
		return surface;
	}

	if(!valid_found) {
		UTWC_LOG(UTWC_LOG_DEBUG, "No applicable gesture bindings for current"
				" touch points");
		touch_ctx.invalidated = true;
		return surface;
	}

	// Track the current finger and tell the calling function not to input to
	// any surfaces.
	touch_ctx.fingers[touch_ctx.fpos++] = (struct finger){
		.id = event->touch_id,
		.start_x = x,
		.start_y = y,
		.x = x,
		.y = y,
	};

	return NULL;
}

struct wlr_surface *utwc_gesture_do_touch_motion(
		struct wlr_event_touch_motion *event, int *ret_x, int *ret_y)
{
	int x, y, sx = 0, sy = 0;
	struct utwc_scene_node *surface_node = NULL;
	struct wlr_surface *surface = NULL;

	*ret_x = 0;
	*ret_y = 0;

	bool no_finger = true;
	struct finger *this_finger = NULL;
	// Check if the finger is part of an ongoing gesture binding.
	for(unsigned int i = 0; i < touch_ctx.fpos; i++) {
		if(event->touch_id == touch_ctx.fingers[i].id) {
			this_finger = &touch_ctx.fingers[i];
			absolute_to_scene_local(event->x, event->y, &this_finger->x,
					&this_finger->y);
			no_finger = false;
			break;
		}
	}

	if(G_output.wlr_output == NULL) {
		touch_ctx.invalidated = true;
		return NULL;
	}

	if(!touch_ctx.invalidated) {
		bool valid_found = false;
		struct utwc_gesture_binding *pos;
		wl_list_for_each(pos, &G_binding.current->gesture, link) {
			if(pos->invalidated)
				continue;

			// Fingers are not allowed to move during a tap.
			if(pos->tap)
				pos->invalidated = true;

			if(pos->invalidated) {
				UTWC_LOG(UTWC_LOG_DEBUG,
						"Gesture does not look like binding \"%s\"",
						pos->human);
			} else {
				valid_found = true;
			}
		}

		if(!valid_found) {
			UTWC_LOG(UTWC_LOG_DEBUG, "No applicable gesture bindings for"
					 " current touch points");
			touch_ctx.invalidated = true;
		}
	}

	// Finger is tracked, don't give input to any surfaces.
	if(!no_finger)
		return NULL;

	absolute_to_scene_local(event->x, event->y, &x, &y);
	surface_at_local_coords(x, y, &surface, &surface_node, &sx, &sy);
	*ret_x = sx;
	*ret_y = sy;

	return surface;
}

bool utwc_gesture_do_touch_up(struct wlr_event_touch_up *event)
{
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	touch_ctx.n_points--;

	if(G_output.wlr_output == NULL)
		touch_ctx.invalidated = true;

	bool on_surface = false;
	unsigned int old_fpos = touch_ctx.fpos;
	bool no_finger = true;
	struct finger this_finger = {
		-1, -1, -1, -1, -1
	};

	struct utwc_scene_node *surface_node = NULL;
	struct wlr_surface *surface = NULL;

	// Check if the finger is part of an ongoing binding.
	for(int i = touch_ctx.fpos - 1; i >= 0; i--) {
		if(event->touch_id == touch_ctx.fingers[i].id) {
			this_finger = touch_ctx.fingers[i];
			// Remove the entry.
			touch_ctx.fingers[i] = touch_ctx.fingers[--touch_ctx.fpos];
			no_finger = false;

			// figure out if the touch up happened on a surface
			int sx, sy;
			surface_at_local_coords(this_finger.x, this_finger.y, &surface,
					&surface_node, &sx, &sy);
			on_surface = (surface != NULL);
			break;
		}
	}

	if(!touch_ctx.invalidated) {
		// All fingers on the screen have to be tracked for this to be run.

		bool valid_found = false;
		struct utwc_gesture_binding *pos;
		wl_list_for_each(pos, &G_binding.current->gesture, link) {
			if(pos->invalidated)
				continue;

			if(!surface_matches_watcher_rule(surface_node,
						pos->surface_end_rule))
				pos->invalidated = true;

			// Check if there are too little fingers for the binding.
			if(!touch_ctx.going_down && old_fpos != pos->fingers)
				pos->invalidated = true;

			enum utwc_gesture_region region = get_region_at(this_finger.x,
					this_finger.y);
			region |= get_relative_region_for( this_finger.start_x,
					this_finger.start_y, this_finger.x, this_finger.y);

			if((region & pos->end) != pos->end)
				pos->invalidated = true;

			if(!time_is_between(&touch_ctx.begin, &now, &pos->min_time,
						&pos->max_time))
				pos->invalidated = true;

			enum utwc_gesture_surface surf = pos->surface_end;
			if(surf != GESTURE_ON_ANY &&
					on_surface != (surf == GESTURE_ON_SURFACE))
				pos->invalidated = true;

			if(pos->invalidated) {
				UTWC_LOG(UTWC_LOG_DEBUG,
						"Gesture does not look like binding \"%s\"",
						pos->human);
			} else {
				valid_found = true;
				UTWC_LOG(UTWC_LOG_DEBUG,
						"Gesture binding \"%s\" went down a finger",
						pos->human);
			}
		}

		if(!valid_found) {
			UTWC_LOG(UTWC_LOG_DEBUG, "No applicable gesture bindings for"
					 " current touch points");
			touch_ctx.invalidated = true;
		}
	}

	touch_ctx.going_down = true;

	// Handle any completed bindings.
	if(!touch_ctx.invalidated && touch_ctx.n_points == 0) {
		struct utwc_gesture_binding *pos;
		wl_list_for_each(pos, &G_binding.current->gesture, link)
			if(!pos->invalidated)
				handle_binding(pos);
	}

	return no_finger;
}

void utwc_gesture_update_for_mode(void)
{
	touch_ctx.invalidated = true;
}
