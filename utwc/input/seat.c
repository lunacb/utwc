#include <stdbool.h>
#include <wayland-server-core.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_primary_selection.h>
#include <wlr/types/wlr_seat.h>
#include "input/gesture.h"
#include "input/keyboard.h"
#include "log.h"
#include "rule.h"
#include "server.h"
#include "input/touch.h"
#include "util.h"

#include "input/seat.h"

struct utwc_G_seat G_seat = { .created = false };

void utwc_process_input(struct wlr_input_device *device)
{
	switch(device->type) {
		case WLR_INPUT_DEVICE_KEYBOARD:
			utwc_keyboard_handle_new(device);
			break;

		case WLR_INPUT_DEVICE_TOUCH:
			utwc_touch_handle_new(device);
			break;

		case WLR_INPUT_DEVICE_TABLET_TOOL:
		case WLR_INPUT_DEVICE_TABLET_PAD:
			UTWC_LOG(UTWC_LOG_INFO, "Ignoring unsupported tablet device %s",
				device->name);
			break;

		case WLR_INPUT_DEVICE_SWITCH:
			break;

		case WLR_INPUT_DEVICE_POINTER:
			UTWC_LOG(UTWC_LOG_INFO, "Ignoring unsupported pointer device %s",
				device->name);
			break;

		default:
			UTWC_LOG(UTWC_LOG_ERROR, "Unkown device type for device %s",
				device->name);
		break;
	}

	uint32_t caps = 0;

	if(!wl_list_empty(&G_seat.keyboards))
		caps |= WL_SEAT_CAPABILITY_KEYBOARD;
	if(G_touch.device)
		caps |= WL_SEAT_CAPABILITY_TOUCH | WL_SEAT_CAPABILITY_POINTER;

	wlr_seat_set_capabilities(G_seat.wlr_seat, caps);
}

static void handle_new_input(struct wl_listener *listener, void *data)
{
	(void)listener;
	utwc_process_input((struct wlr_input_device *)data);
}

static void handle_new_virtual_keyboard(struct wl_listener *listener, void *data)
{
	(void)listener;
	struct wlr_virtual_keyboard_v1 *keyboard = data;
	utwc_process_input(&keyboard->input_device);
}

static void handle_request_cursor(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	//TODO: handle
}

static void handle_request_set_selection(struct wl_listener *listener, void *data)
{
	(void)listener;

	struct wlr_seat_request_set_selection_event *event = data;
	wlr_seat_set_selection(G_seat.wlr_seat, event->source, event->serial);
}
static void handle_request_set_primary_selection(struct wl_listener *listener, void *data)
{
	(void)listener;

	struct wlr_seat_request_set_primary_selection_event *event = data;
	wlr_seat_set_primary_selection(G_seat.wlr_seat, event->source, event->serial);
}

static void handle_request_start_drag(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	//TODO: handle
}

static void handle_start_drag(struct wl_listener *listener, void *data)
{
	(void)listener;
	(void)data;
	//TODO: handle
}

bool utwc_G_seat_init(void)
{
	G_seat.created = true;
	G_touch.device = NULL;
	G_touch.has_pointer_id = false;
	G_touch.pointer_id = 0;
	wl_list_init(&G_touch.pending_devices);
	wl_list_init(&G_seat.keyboards);

	G_seat.wlr_seat = wlr_seat_create(G_server.wl_display, "seat0");
	G_seat.layer = NULL;


	HANDLE_SIGNAL(G_seat.e_new_input,
			handle_new_input,
			G_server.wlr_backend->events.new_input)
	HANDLE_SIGNAL(G_seat.e_new_virtual_keyboard,
			handle_new_virtual_keyboard,
			G_server.wlr_virtual_keyboard_manager->events.new_virtual_keyboard)
	HANDLE_SIGNAL(G_seat.e_request_cursor,
			handle_request_cursor,
			G_seat.wlr_seat->events.request_set_cursor)
	HANDLE_SIGNAL(G_seat.e_request_set_selection,
			handle_request_set_selection,
			G_seat.wlr_seat->events.request_set_selection)
	HANDLE_SIGNAL(G_seat.e_request_set_primary_selection,
			handle_request_set_primary_selection,
			G_seat.wlr_seat->events.request_set_primary_selection)
	HANDLE_SIGNAL(G_seat.e_request_start_drag,
			handle_request_start_drag,
			G_seat.wlr_seat->events.request_start_drag)
	HANDLE_SIGNAL(G_seat.e_start_drag,
			handle_start_drag,
			G_seat.wlr_seat->events.start_drag)

	return true;
}

void utwc_G_seat_fini(void)
{
	if(!G_seat.created)
		return;

	free_list(&G_touch.pending_devices, link, struct utwc_pending_touch);
	struct utwc_keyboard *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &G_seat.keyboards, link)
		utwc_keyboard_fini(pos);
}

bool seat_can_focus_keyboard(struct utwc_rule_target *rule_target)
{
	if(rule_target->type != UTWC_RULE_VIEW || G_seat.layer != NULL)
		return false;

	return true;
}
