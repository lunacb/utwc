#include <stddef.h>
#include <string.h>
#include <xkbcommon/xkbcommon.h>
#include "command.h"
#include "input/binding.h"
#include "log.h"
#include "util.h"

#include "input/keybind.h"

bool utwc_keybind_add_binding(struct utwc_keybinding *binding,
		struct utwc_binding_mode *mode)
{
	struct utwc_keybinding *new = utwc_malloc(sizeof(*new));
	*new = *binding;
	strcpy(new->human, "TODO");
	wl_list_insert(&mode->keybind, &new->link);

	return false;
}

void utwc_keybind_fini(struct utwc_keybinding *binding)
{
	utwc_command_chain_fini(&binding->command);

	wl_list_remove(&binding->link);
	free(binding);
}

static void handle_binding(struct utwc_keybinding *binding)
{
	UTWC_LOG(UTWC_LOG_DEBUG, "Handle keybinding \"%s\"", binding->human);
	utwc_command_chain_execute(&binding->command);
}

bool utwc_keybind_do_key(const xkb_keysym_t *keysyms, size_t nsyms,
		uint32_t modifiers)
{
	struct utwc_keybinding *pos;
	for(unsigned int i = 0; i < nsyms; i++) {
		wl_list_for_each(pos, &G_binding.current->keybind, link) {
			if(pos->keysym != keysyms[i]) continue;
			if(pos->modifiers != modifiers) continue;

			handle_binding(pos);
			return true;
		}
	}
	return false;
}
