#define _POSIX_C_SOURCE 200809L

#include <stdbool.h>
#include <string.h>
#include <wayland-server-core.h>
#include "input/gesture.h"
#include "input/keybind.h"
#include "util.h"

#include "input/binding.h"

struct utwc_G_binding G_binding = { .created = false };

void utwc_binding_mode_enter(struct utwc_binding_mode *mode)
{
	if(mode == G_binding.current)
		return;

	G_binding.current = mode;
	utwc_gesture_update_for_mode();
}

void utwc_binding_mode_remove(struct utwc_binding_mode *mode)
{
	// Silently refuse to remove if this is the initial mode.
	if(&mode->link == G_binding.modes.next)
		return;

	// Enter another binding if the to-be-removed one is current.
	if(mode == G_binding.current) {
		struct utwc_binding_mode *first =
			wl_container_of(G_binding.modes.next, first, link);
		utwc_binding_mode_enter(first);
	}

	wl_list_remove(&mode->link);
	struct utwc_keybinding *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &mode->keybind, link)
		utwc_keybind_fini(pos);
	struct utwc_gesture_binding *gpos, *gtmp;
	wl_list_for_each_safe(gpos, gtmp, &mode->keybind, link)
		utwc_gesture_fini(gpos);
	free(mode);
}

bool utwc_binding_name_enter(char *mode)
{
	struct utwc_binding_mode *pos;
	wl_list_for_each(pos, &G_binding.modes, link) {
		if(!strcmp(mode, pos->name)) {
			utwc_binding_mode_enter(pos);
			return true;
		}
	}

	return false;
}

bool utwc_binding_name_remove(char *mode)
{
	struct utwc_binding_mode *pos;
	wl_list_for_each(pos, &G_binding.modes, link) {
		if(!strcmp(mode, pos->name)) {
			utwc_binding_mode_remove(pos);
			return true;
		}
	}

	return false;
}

struct utwc_binding_mode *find_or_create_mode(char *name, char *title)
{
	struct utwc_binding_mode *pos;
	wl_list_for_each(pos, &G_binding.modes, link) {
		if(!strcmp(name, pos->name)) {
			// Set the found modes' title to the given title.
			free(pos->title);
			if(title == NULL)
				pos->title = NULL;
			else
				pos->title = utwc_strdup(title);

			return pos;
		}
	}

	struct utwc_binding_mode *binding = utwc_malloc(sizeof(*binding));
	*binding = (struct utwc_binding_mode){
		.name = utwc_strdup(name),
		.title = (title != NULL)?utwc_strdup(title):NULL,
	};
	wl_list_insert(&G_binding.modes, &binding->link);

	return binding;
}

bool utwc_G_binding_init(void)
{
	G_binding.created = true;

	struct utwc_binding_mode *normal = utwc_malloc(sizeof(*normal));
	*normal = (struct utwc_binding_mode){
		.name = utwc_strdup("normal"),
		.title = NULL,
	};
	wl_list_init(&normal->gesture);
	wl_list_init(&normal->keybind);

	wl_list_init(&G_binding.modes);
	wl_list_insert(&G_binding.modes, &normal->link);

	G_binding.current = normal;

	return true;
}

void utwc_G_binding_fini(void)
{
	struct utwc_binding_mode *pos, *tmp;
	wl_list_for_each_safe(pos, tmp, &G_binding.modes, link) {
		free(pos->name);
		free(pos->title);
		struct utwc_gesture_binding *gpos, *gtmp;
		wl_list_for_each_safe(gpos, gtmp, &pos->gesture, link)
			utwc_gesture_fini(gpos);
		struct utwc_keybinding *kpos, *ktmp;
		wl_list_for_each_safe(kpos, ktmp, &pos->gesture, link)
			utwc_keybind_fini(kpos);
		free(pos);
	}
}
