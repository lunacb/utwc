#define _POSIX_C_SOURCE 201112L

#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "log.h"

static struct timespec start_time = {-1, -1};
static enum utwc_log_verbosity log_verb = UTWC_LOG_ERROR;

static const struct {
	const char *color;
	const char *name;
} verb_info[] = {
	{"", ""},
	{"\x1B[1;31m", "[ERROR]"},
	{"\x1B[1;34m", "[INFO]"},
	{"\x1B[1;90m", "[DEBUG]"}
};

void utwc_log_init(enum utwc_log_verbosity new_verb)
{
	clock_gettime(CLOCK_MONOTONIC, &start_time);
	log_verb = new_verb;
}

void utwc_log_va(enum utwc_log_verbosity verb,
		const char *file, int line,
		const char *format, va_list args)
{
	if(verb > log_verb)
		return;

	bool color = isatty(STDERR_FILENO);
	struct timespec diff = {0};
	clock_gettime(CLOCK_MONOTONIC, &diff);
	diff.tv_sec -= start_time.tv_sec;
	if(diff.tv_nsec >= start_time.tv_nsec) {
		diff.tv_nsec -= start_time.tv_nsec;
	} else {
		diff.tv_sec--;
		diff.tv_nsec = 1000000000 - start_time.tv_nsec + diff.tv_nsec;
	}

	fprintf(stderr, "%02d:%02d:%02d.%03ld ",
		(int)(diff.tv_sec / 60 / 60),
		(int)(diff.tv_sec / 60 % 60),
		(int)(diff.tv_sec % 60),
		(long)(diff.tv_nsec / 1000000));

	if(color)
		fputs(verb_info[verb].color, stderr);
	else
		fputs(verb_info[verb].name, stderr);

	if(file)
		fprintf(stderr, "[%s:%d] ", file, line);
	vfprintf(stderr, format, args);

	fputs((color)?"\x1B[0m\n":"\n", stderr);
}

void utwc_log(enum utwc_log_verbosity verb,
		const char *file, int line,
		const char *format, ...)
{
	va_list args;
	va_start(args, format);
	utwc_log_va(verb, file, line, format, args);
	va_end(args);
}
