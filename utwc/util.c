#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-server-core.h>
#include "log.h"
#include "server.h"

#include "util.h"

void *alloc_test(const char *file, int line, void *ptr)
{
	if(ptr == NULL) {
		utwc_log(UTWC_LOG_ERROR, file, line, "Failed to allocate memory: %s",
				strerror(errno));
		// try to exit nicely
		if(G_server.wl_display)
			wl_display_terminate(G_server.wl_display);
	}
	return ptr;
}

void free_list_offst(struct wl_list *head, size_t offst)
{
	struct wl_list *pos, *tmp;

	//wl_list_for_each_safe but iterating over links instead of their
	//underlying structs
	for(pos = head->next, tmp = pos->next;
			pos != head;
			pos = tmp, tmp = pos->next) {
		free((void *)((void *)pos - (void *)offst));
	}

	wl_list_init(head);
}
