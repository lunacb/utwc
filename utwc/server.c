#define _POSIX_C_SOURCE 201112L

#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/allocator.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_data_control_v1.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_export_dmabuf_v1.h>
#include <wlr/types/wlr_gamma_control_v1.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_presentation_time.h>
#include <wlr/types/wlr_primary_selection_v1.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_viewporter.h>
#include <wlr/types/wlr_virtual_keyboard_v1.h>
#include <wlr/types/wlr_xdg_output_v1.h>
#include <wlr/types/wlr_xdg_shell.h>
#include "command.h"
#include "desktop.h"
#include "input/binding.h"
#include "input/seat.h"
#include "layers.h"
#include "log.h"
#include "output.h"
#include "pid.h"
#include "scene.h"
#include "util.h"

#include "server.h"

struct utwc_G_server G_server;

void handle_signal(int signal)
{
	(void)signal;

	if(signal == SIGINT || signal == SIGTERM) {
		if(G_server.wl_display)
			wl_display_terminate(G_server.wl_display);
	}
	if(signal == SIGCHLD)
		utwc_pid_wait();
}

bool utwc_server_init(void)
{
	G_server.wl_display = NULL;
	G_server.wlr_backend = NULL;
	G_server.wlr_compositor = NULL;
	G_server.wlr_renderer = NULL;
	G_server.wlr_xdg_shell = NULL;

	G_server.wl_display = wl_display_create();
	if(!(G_server.wlr_backend = wlr_backend_autocreate(G_server.wl_display)))
			goto fail;

	G_server.wlr_renderer = wlr_renderer_autocreate(G_server.wlr_backend);
	wlr_renderer_init_wl_display(G_server.wlr_renderer, G_server.wl_display);
	G_server.wlr_allocator = wlr_allocator_autocreate(G_server.wlr_backend,
			G_server.wlr_renderer);
	G_server.wlr_compositor = wlr_compositor_create(G_server.wl_display,
		G_server.wlr_renderer);
	G_server.wlr_xdg_shell = wlr_xdg_shell_create(G_server.wl_display);
	G_server.wlr_layer_shell = wlr_layer_shell_v1_create(G_server.wl_display);

	wlr_data_device_manager_create(G_server.wl_display);
	wlr_gamma_control_manager_v1_create(G_server.wl_display);
	wlr_export_dmabuf_manager_v1_create(G_server.wl_display);
	wlr_screencopy_manager_v1_create(G_server.wl_display);
	wlr_data_control_manager_v1_create(G_server.wl_display);
	wlr_primary_selection_v1_device_manager_create(G_server.wl_display);
	wlr_viewporter_create(G_server.wl_display);
	G_server.wlr_virtual_keyboard_manager =
		wlr_virtual_keyboard_manager_v1_create(G_server.wl_display);
	G_server.wlr_presentation = wlr_presentation_create(G_server.wl_display,
		G_server.wlr_backend);
	// for xdg_output_manager
	G_server.wlr_output_layout = wlr_output_layout_create();
	G_server.wlr_xdg_output_manager =
		wlr_xdg_output_manager_v1_create(G_server.wl_display,
				G_server.wlr_output_layout);


	if(!utwc_G_output_init()) goto fail;
	if(!utwc_G_seat_init()) goto fail;
	if(!utwc_G_binding_init()) goto fail;
	if(!utwc_G_scene_init()) goto fail;
	if(!utwc_G_layers_init()) goto fail;
	if(!utwc_G_desktop_init()) goto fail;
	if(!utwc_G_command_init()) goto fail;

	/*TODO: unimplemented wlr features
	 *	wlr_idle_inhibit_manager_v1
	 *	wlr_tablet_v2 (maybe)
	 *	wlr_server_decoration_manager
	 *	wlr_xdg_decoration_manager_v1
	 *	wlr_relative_pointer_manager_v1
	 *	wlr_pointer_constraints_v1
	 *	wlr_output_power_manager_v1
	 *	wlr_input_method_manager_v2
	 *	wlr_text_input_manager_v3
	 *	wlr_drm_lease_v1_manager
	 *	wlr_xdg_foreign_...
	 *	TODO: this list may not be accurate to wlroots 0.15.1
	*/

	const char *socket = wl_display_add_socket_auto(G_server.wl_display);
	if(!socket)
		goto fail;

	if(!wlr_backend_start(G_server.wlr_backend))
		goto fail;

	//TODO: test for XDG_RUNTIME_DIR
	setenv("WAYLAND_DISPLAY", socket, true);

	signal(SIGTERM, handle_signal);
	signal(SIGINT, handle_signal);
	signal(SIGCHLD, handle_signal);

	return true;
fail:
	utwc_server_destroy();

	return false;
}

void utwc_server_run(void)
{
	wl_display_run(G_server.wl_display);
}

void utwc_server_destroy(void)
{
	if(G_server.wlr_output_layout)
		wlr_output_layout_destroy(G_server.wlr_output_layout);

	wlr_renderer_destroy(G_server.wlr_renderer);
	wlr_allocator_destroy(G_server.wlr_allocator);

	if(G_scene.created)
		utwc_scene_node_destroy(&G_scene.node);

	if(G_server.wl_display) {
		wl_display_destroy_clients(G_server.wl_display);
		wl_display_destroy(G_server.wl_display);
	}

	utwc_G_seat_fini();
	utwc_G_binding_fini();
	utwc_G_desktop_fini();
	utwc_G_command_fini();
}
