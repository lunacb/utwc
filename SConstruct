import os

vars = Variables('variables.py', ARGUMENTS)
vars.AddVariables(
        BoolVariable('werror',
            help='True if the -Werror c compiler option should be set',
            default=False
        ),
        BoolVariable('verbose',
            'True if full program compilation output should be shown',
            default=False
        ),
        EnumVariable('target',
            help='Compilation target',
            default='debug',
            allowed_values = ('debug', 'release'),
        ),
)

base_env = Environment(variables=vars)

Help(vars.GenerateHelpText(base_env))

base_env.Decider('MD5-timestamp')

if base_env['werror']:
    base_env.MergeFlags('-Werror')

if not(base_env['verbose']):
    def comstr(color, no_color):
        return "\033[32m%s\033[0m %s" % (color, no_color)
    base_env.Append(CCCOMSTR=comstr("Compiling", "$TARGET"))
    base_env.Append(CXXCOMSTR=comstr("Compiling", "$TARGET"))
    base_env.Append(SHCCCOMSTR=comstr("Compiling shared", "$TARGET"))
    base_env.Append(SHCXXCOMSTR=comstr("Compiling shared", "$TARGET"))
    base_env.Append(ARCOMSTR=comstr("Linking static library ", "$TARGET"))
    base_env.Append(RANLIBCOMSTR=comstr("Ranlib library", "$TARGET"))
    base_env.Append(SHLINKCOMSTR=comstr("Linking shared library", "$TARGET"))
    base_env.Append(LINKCOMSTR=comstr("Linking", "$TARGET"))

build_dir = 'build/release/'
if base_env['target'] == 'debug':
    base_env.MergeFlags('-g')
    base_env.MergeFlags('-Og')
    build_dir = 'build/debug/'
else:
    base_env.MergeFlags('-O3')

base_env.Append(CPPPATH=['#/include'])
base_env.Append(CPPPATH=['#/'+build_dir+'protocols'])

base_env.Append(CPPDEFINES=['WLR_USE_UNSTABLE'])

base_env.MergeFlags('''
        -Wall
        -Winvalid-pch
        -Wextra
        -std=c11
        -fdiagnostics-color=always
        ''')


server_env = base_env.Clone()

Export('base_env')
Export('server_env')

conscripts = [
    "protocols",
    "utwc"
]

cleaning = GetOption('clean')
for c in conscripts:
    if cleaning: # clean all build dirs instead of just the one for the current target
        base_env.SConscript(c + '/SConscript', variant_dir='build/release/' + c, duplicate=False)
        base_env.SConscript(c + '/SConscript', variant_dir='build/debug/' + c, duplicate=False)
    else:
        base_env.SConscript(c + '/SConscript', variant_dir=build_dir + c, duplicate=False)
